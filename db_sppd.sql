/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100408
 Source Host           : localhost:3306
 Source Schema         : db_sppd

 Target Server Type    : MySQL
 Target Server Version : 100408
 File Encoding         : 65001

 Date: 17/02/2020 10:31:51
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tb_anggaran
-- ----------------------------
DROP TABLE IF EXISTS `tb_anggaran`;
CREATE TABLE `tb_anggaran`  (
  `id_anggaran` int(2) NOT NULL AUTO_INCREMENT,
  `uang_makan` double(10, 0) NULL DEFAULT NULL,
  `uang_saku` double(10, 0) NULL DEFAULT NULL,
  `uang_penginapan` double(10, 0) NULL DEFAULT NULL,
  `uang_transport` double(10, 0) NULL DEFAULT NULL,
  `id_jabatan` int(3) NULL DEFAULT NULL,
  PRIMARY KEY (`id_anggaran`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_anggaran
-- ----------------------------
INSERT INTO `tb_anggaran` VALUES (1, 100000, 100000, 100000, 200000, 6);
INSERT INTO `tb_anggaran` VALUES (2, 50000, 50000, 50000, 200000, 3);
INSERT INTO `tb_anggaran` VALUES (4, 200000, 300000, 500000, 200000, 4);
INSERT INTO `tb_anggaran` VALUES (5, 100000, 120000, 500000, 200000, 5);
INSERT INTO `tb_anggaran` VALUES (6, 200000, 200000, 200000, 200000, 12);

-- ----------------------------
-- Table structure for tb_divisi
-- ----------------------------
DROP TABLE IF EXISTS `tb_divisi`;
CREATE TABLE `tb_divisi`  (
  `id_divisi` int(3) NOT NULL AUTO_INCREMENT,
  `nama_divisi` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `keterangan` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_divisi`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_divisi
-- ----------------------------
INSERT INTO `tb_divisi` VALUES (1, 'Operasional', 'Bagian Operasional');
INSERT INTO `tb_divisi` VALUES (2, 'Marketing', 'Bagian Pemasaran Produk');
INSERT INTO `tb_divisi` VALUES (5, 'HRD', 'Menangani Karyawan');
INSERT INTO `tb_divisi` VALUES (6, 'Keuangan', 'Finance, Operasional Kantor dl');
INSERT INTO `tb_divisi` VALUES (7, 'Elektro', 'Jaringan');
INSERT INTO `tb_divisi` VALUES (8, 'Direktur', '-');

-- ----------------------------
-- Table structure for tb_jabatan
-- ----------------------------
DROP TABLE IF EXISTS `tb_jabatan`;
CREATE TABLE `tb_jabatan`  (
  `id_jabatan` int(3) NOT NULL AUTO_INCREMENT,
  `nama_jabatan` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `keterangan` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_jabatan`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_jabatan
-- ----------------------------
INSERT INTO `tb_jabatan` VALUES (1, 'Direktur', 'Owner');
INSERT INTO `tb_jabatan` VALUES (3, 'Staff IT', '-');
INSERT INTO `tb_jabatan` VALUES (4, 'Manager IT', '-');
INSERT INTO `tb_jabatan` VALUES (5, 'Staff Operasional', '-');
INSERT INTO `tb_jabatan` VALUES (6, 'Manager Operasional', '-');
INSERT INTO `tb_jabatan` VALUES (7, 'Staff Marketing', '-');
INSERT INTO `tb_jabatan` VALUES (8, 'Manager Marketing', '-');
INSERT INTO `tb_jabatan` VALUES (9, 'Staff HRD', '-');
INSERT INTO `tb_jabatan` VALUES (10, 'Manager HRD', '-');
INSERT INTO `tb_jabatan` VALUES (11, 'Manager Keuangan', 'Kepala Bidang Keuangan');
INSERT INTO `tb_jabatan` VALUES (12, 'Staff Keuangan', '-');
INSERT INTO `tb_jabatan` VALUES (13, 'Manager Elektro', '-');
INSERT INTO `tb_jabatan` VALUES (14, 'Office Boy', '-');
INSERT INTO `tb_jabatan` VALUES (15, 'Staff Elektro', '-');

-- ----------------------------
-- Table structure for tb_karyawan
-- ----------------------------
DROP TABLE IF EXISTS `tb_karyawan`;
CREATE TABLE `tb_karyawan`  (
  `nik` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '0',
  `nama_karyawan` varchar(40) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tempat_lahir` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_lahir` date NULL DEFAULT NULL,
  `alamat` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `jenis_kelamin` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `agama` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status_karyawan` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `no_telp` varchar(13) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `no_telp_darurat` varchar(13) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_masuk_kerja` date NULL DEFAULT NULL,
  `email` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nomor_ktp` int(16) NULL DEFAULT NULL,
  `npwp` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nomor_rekening` int(20) NULL DEFAULT NULL,
  `id_divisi` int(3) NULL DEFAULT NULL,
  `id_jabatan` int(3) NULL DEFAULT NULL,
  PRIMARY KEY (`nik`) USING BTREE,
  INDEX `id_divisi`(`id_divisi`) USING BTREE,
  INDEX `id_jabatan`(`id_jabatan`) USING BTREE,
  CONSTRAINT `tb_karyawan_ibfk_1` FOREIGN KEY (`id_divisi`) REFERENCES `tb_divisi` (`id_divisi`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tb_karyawan_ibfk_2` FOREIGN KEY (`id_jabatan`) REFERENCES `tb_jabatan` (`id_jabatan`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_karyawan
-- ----------------------------
INSERT INTO `tb_karyawan` VALUES ('200911060001', 'Nia Olivia Widaryanti', 'Jakarta', '1982-11-14', 'Jl. Merak Blok 21 No 4 Pjmi Rt/Rw 2/7 Kel. Jurangmangu Timur Kec. Pondok Aren - Tangerang Selatan', 'Perempuan', 'Protestan', 'Sudah Menikah', '081806078764', '08891550029', '2009-11-06', 'nia@pasperkasa.co', NULL, NULL, NULL, 8, 1);
INSERT INTO `tb_karyawan` VALUES ('201002220002', 'Yuddy Marcelino Tambaritji', 'Pontianak', '1978-03-18', 'Jl. Merak Blok 21 No 4 Pjmi Rt/Rw 2/7 Kel. Jurangmangu Timur Kec. Pondok Aren - Tangerang Selatan', 'Laki-laki', 'Protestan', 'Sudah Menikah', '08891550029', '08106078764', '1978-03-18', 'marcel@pasperkasa.co', NULL, NULL, NULL, 6, 11);
INSERT INTO `tb_karyawan` VALUES ('201105180003', 'Umayah', 'Kebumen', '1991-05-20', 'DK. Krajan Kulon Rt/Rw.04/02 Kel. Sadangkulon Kec. Sadang - Kebumen', 'Perempuan', 'Islam', 'Sudah Menikah', '087878870309', '081511441118', '2011-05-18', 'umayah@pasperkasa.co', NULL, NULL, NULL, 1, 5);
INSERT INTO `tb_karyawan` VALUES ('201208010004', 'Ronni Parulian Siregar', 'Jakarta', '1984-03-02', 'Jl. Merak Blok 21 No 4 Pjmi Rt/Rw 2/7 Kel. Jurangmangu Timur Kec. Pondok Aren - Tangerang Selatan', 'Laki-laki', 'Protestan', 'Sudah Menikah', '081289265065', '08125551234', '2012-08-01', 'ronni@pasperkasa.co', NULL, NULL, NULL, 1, 6);
INSERT INTO `tb_karyawan` VALUES ('201405300005', 'Aleineke Selani Rampen', 'Manado', '1981-08-14', 'BTN Nusantara Permai Blok B3-4 Lingk Vi Mapanget Barat Rw 6 Kel. Mapanget Barat Kec. Mapanget-Manado', 'Perempuan', 'Protestan', 'Belum Menikah', '081905514401', '085214975884', '2014-05-30', 'alein@pasperkasa.co', NULL, NULL, NULL, 7, 13);
INSERT INTO `tb_karyawan` VALUES ('201408260006', 'Witriany Mamontoh', 'Manado', '1979-05-13', 'Paniki Bawah Lingk. Ix Rw. 9 Kel. Paniki Bawah Kec. Mapanget - Manado - Sulawesi Utara', 'Perempuan', 'Islam', 'Belum Menikah', '081340129879', '082189461182', '2014-08-26', 'riska@mail.com', NULL, NULL, NULL, 5, 10);
INSERT INTO `tb_karyawan` VALUES ('201408260007', 'Mislam Sukentri', 'Banyumas', '1993-02-18', 'Salandaka Rt/Rw.01/02 Kel. Salandaka, Kec. Sempiuh, Kab. Banyumas - Jawa Tengah', 'Laki-laki', 'Islam', 'Belum Menikah', '081905632659', '085966116174', '2014-08-26', 'kentri@pasperkasa.co', NULL, NULL, NULL, 5, 14);
INSERT INTO `tb_karyawan` VALUES ('201501090009', 'Evi Anjarsari', 'Medan', '1987-04-14', 'Rusun Dakota 1B/403 RT. 001 RW. 011 Kebon Kosong Kemayoran', 'Laki-laki', 'Islam', 'Sudah Menikah', '081269221633', '', '2015-09-01', 'evi@pasperkasa.co', NULL, NULL, NULL, 2, 7);
INSERT INTO `tb_karyawan` VALUES ('201506250008', 'Riska Damayanti', 'Bogor', '1996-03-09', 'Jl. Abus Rt/Rw. 01/03 Kel. Meruyung Kec. Limo - Depok - Jawa Barat', 'Perempuan', 'Islam', 'Belum Menikah', '081511441118', '083806121685', '2015-06-25', 'riska@pasperkasa.co', NULL, NULL, NULL, 6, 12);
INSERT INTO `tb_karyawan` VALUES ('201509010010', 'Yuyun Eka Supriyatna', 'Surabaya', '1986-06-07', 'Jl. Lumbu Barat V C /199 Blok 7 Rt/Rw. 06/10 Kel. Bojong Rawalumbu Kec. Rawalumbu - Bekasi', 'Perempuan', 'Islam', 'Sudah Menikah', '082312004881', '085691123387', '2015-09-01', 'yuyun@pasperkasa.co', NULL, NULL, NULL, 2, 7);
INSERT INTO `tb_karyawan` VALUES ('201510230011', 'Zahra Sakti Saputro', 'Sukoharjo', '1990-10-01', 'DK. Kauman Rt/Rw.01/06 Kel. Weru Kec. Weru Kab. Sukoharjo - Jawa Tengah', 'Laki-laki', 'Islam', 'Belum Menikah', '085640007901', '081548769967', '2015-10-23', 'sakti@pasperkas.co', NULL, NULL, NULL, 1, 5);
INSERT INTO `tb_karyawan` VALUES ('201511260012', 'Ferdinanda Anaktototy', 'Ambon', '1981-07-04', 'Jl ADM Negara I Blok 4 L 5/16 Rt/Rw 7/11 Kel. Petamburan Kec.Tanah Abang - Jakarta Pusat', 'Perempuan', 'Protestan', 'Sudah Menikah', '081310102331', '081398974669', '2018-11-26', 'wanda@gmail.com', NULL, NULL, NULL, 5, 9);
INSERT INTO `tb_karyawan` VALUES ('201512300013', 'Kasihardo Herlambang', 'Denpasar', '1989-05-02', 'Jl Jakarta B5/26 Gjr Rt/Rw 3/14 Kel. Japan Kec. Sooko Kab. Mojokerto - Jawa Timur', 'Laki-laki', 'Protestan', 'Sudah Menikah', '081290442016', '0818564823', '2015-12-30', 'aldo@pasperkasa.co', NULL, NULL, NULL, 2, 8);
INSERT INTO `tb_karyawan` VALUES ('201603010014', 'Gunawan', 'Jakarta', '1970-04-30', 'Jl. Garuda Bawah No. 1 Rt/Rw.08/12 Kel. Bintaro Kec. Pesanggrahan - Jakarta Selatan', 'Laki-laki', 'Islam', 'Sudah Menikah', '08151684234', '08888989937', '2016-03-01', 'gunawan@pasperkasa.co', NULL, NULL, NULL, 7, 15);
INSERT INTO `tb_karyawan` VALUES ('201605160015', 'Cahyo Rona Wijanarko', 'Sragen', '1990-01-05', 'Mageru Rt/Rw.03/01 Kel. Sragen Tengah Kec. Sragen Kab. Sragen - Jawa Tengah', 'Laki-laki', 'Islam', 'Belum Menikah', '085737383738', '085728166066', '2016-05-16', 'cahyo@pasperkasa.co', NULL, NULL, NULL, 1, 5);
INSERT INTO `tb_karyawan` VALUES ('201605160016', 'Tria Puji Astuti', 'Jakarta', '1992-04-27', 'Cawang Iii Rt 4 Rw 11 Kel Cawang Kec Kramatjati Jakarta Timur', 'Perempuan', 'Islam', 'Belum Menikah', '088212069763', '085776113356', '2016-05-16', 'tria@pasperkasa.co', NULL, NULL, NULL, 6, 12);
INSERT INTO `tb_karyawan` VALUES ('201610260017', 'Walidin Miswanto', 'Jakarta', '1982-07-22', 'Jl. Nusa Indah I/Ix/407 Rt/Rw.07/05 Kel. Malaka Jaya Kec. Duren Sawit - Jakarta Timur', 'Laki-laki', 'Islam', 'Sudah Menikah', '085933716846', '', '2016-10-26', 'walidin@pasperkasa.co', NULL, NULL, NULL, 7, 15);
INSERT INTO `tb_karyawan` VALUES ('201705260018', 'Heru Siregar', 'Jakarta', '1992-06-05', 'Jl. H Gemon Pondok Kelapa Rt/Rw. 04/01 Kel. Pondok Kelapa Kec. Duren Sawit - Jakarta Timur', 'Laki-laki', 'Protestan', 'Belum Menikah', '081299212505', '', '2016-05-26', 'heru@pasperkasa.co', NULL, NULL, NULL, 5, 9);
INSERT INTO `tb_karyawan` VALUES ('201801030019', 'Noviawati', 'Jakarta', '1980-06-22', 'Jl. Palem Ganda Asri Meruyung Limo Depok', 'Perempuan', 'Islam', 'Sudah Menikah', '081387999873', '', '2018-01-03', 'via@pasperkasa.co', NULL, NULL, NULL, 2, 7);
INSERT INTO `tb_karyawan` VALUES ('201805230004', 'admin', 'bogor', '1991-11-28', 'Jl. Madura', 'Laki-laki', 'Islam', 'Sudah Menikah', '021231323123', '021233232333', '2018-05-31', 'admin@mail.com', NULL, NULL, NULL, 2, 4);
INSERT INTO `tb_karyawan` VALUES ('201808060020', 'hgn', 'hgfn', '2018-08-29', 'thn', 'Laki-laki', 'Katolik', 'Sudah Menikah', '5454', '55', '2018-08-23', 'ghn@gmail.om', NULL, NULL, NULL, 6, 5);
INSERT INTO `tb_karyawan` VALUES ('201808070021', 'Supiyanti', 'Bogor', '2009-07-06', 'Jalam kdfuikdk', 'Perempuan', 'Islam', 'Sudah Menikah', '090837488990', '458909034088', '2018-08-01', 'supi@gmail.com', NULL, NULL, NULL, 5, 9);
INSERT INTO `tb_karyawan` VALUES ('201808070022', 'Isma', 'Jakarta', '2018-08-31', 'Jlsfjcjk,dv', NULL, 'Islam', NULL, '325435657', '8798990998', '2018-08-07', 'isma@pasperkasa.co', NULL, NULL, NULL, 7, 15);

-- ----------------------------
-- Table structure for tb_lpd
-- ----------------------------
DROP TABLE IF EXISTS `tb_lpd`;
CREATE TABLE `tb_lpd`  (
  `id_lpd` int(2) NOT NULL AUTO_INCREMENT,
  `kegiatan` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `tempat` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tanggal_kegiatan` date NULL DEFAULT NULL,
  `laporan` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `keterangan` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `klaim` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_datetime` datetime(0) NULL DEFAULT NULL,
  `nik` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_lpd`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_lpd
-- ----------------------------
INSERT INTO `tb_lpd` VALUES (1, 'tes 1', 'tes 1', '2020-01-29', 'tes 1', 'tes 1', 'lpd_1579162655.jpeg', NULL, '201805230004');

-- ----------------------------
-- Table structure for tb_memo_dinas
-- ----------------------------
DROP TABLE IF EXISTS `tb_memo_dinas`;
CREATE TABLE `tb_memo_dinas`  (
  `id_memo` int(2) NOT NULL AUTO_INCREMENT,
  `pengirim` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `nomor_memo` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tanggal` date NULL DEFAULT NULL,
  `lampiran` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_by` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_datetime` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id_memo`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_memo_dinas
-- ----------------------------
INSERT INTO `tb_memo_dinas` VALUES (2, 'Doni', '0101AS', '2020-01-28', 'memo_0101AS.pdf', NULL, NULL);
INSERT INTO `tb_memo_dinas` VALUES (3, 'asddd', 'asd', '2020-01-22', 'memo_asd.jpeg', NULL, NULL);
INSERT INTO `tb_memo_dinas` VALUES (4, 'Joko', 'M/0001', '2020-01-15', 'memo_M0001.docx', NULL, NULL);
INSERT INTO `tb_memo_dinas` VALUES (5, 'ggg', 'ggg', '2020-02-27', NULL, '200911060001', '2020-02-17 10:11:39');
INSERT INTO `tb_memo_dinas` VALUES (6, 'tes123', 'tes123', '2020-02-27', NULL, '200911060001', '2020-02-17 10:12:16');
INSERT INTO `tb_memo_dinas` VALUES (7, 'joni', 'nomor123', '2020-02-27', NULL, '200911060001', '2020-02-17 10:17:28');
INSERT INTO `tb_memo_dinas` VALUES (8, 'asdasd', 'adsasd', '2020-02-28', 'memo_adsasd.jpg', '200911060001', '2020-02-17 10:30:25');

-- ----------------------------
-- Table structure for tb_page
-- ----------------------------
DROP TABLE IF EXISTS `tb_page`;
CREATE TABLE `tb_page`  (
  `page_id` int(3) NOT NULL AUTO_INCREMENT,
  `page_name` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `page_label` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `page_icon` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `category_id` int(3) NOT NULL,
  `page_parent_id` int(3) NOT NULL,
  `page_active` int(2) NULL DEFAULT 1,
  `page_target_blank` int(2) NULL DEFAULT 0,
  `page_sort` int(2) NULL DEFAULT NULL,
  PRIMARY KEY (`page_id`) USING BTREE,
  UNIQUE INDEX `a`(`page_id`) USING BTREE,
  INDEX `b`(`page_name`, `page_label`, `page_icon`, `category_id`, `page_parent_id`, `page_active`, `page_target_blank`, `page_sort`) USING BTREE,
  INDEX `category_id`(`category_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 35 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_page
-- ----------------------------
INSERT INTO `tb_page` VALUES (28, 'admin/Anggaran', 'Data Anggaran', 'fa fa-money', 1, 0, 1, 0, 8);
INSERT INTO `tb_page` VALUES (2, 'admin/c_divisi', 'Data Divisi', 'fa fa-university', 1, 0, 1, 0, 1);
INSERT INTO `tb_page` VALUES (3, 'admin/c_jabatan', 'Data Jabatan', 'fa fa-certificate', 1, 0, 1, 0, 2);
INSERT INTO `tb_page` VALUES (1, 'admin/c_karyawan', 'Data Karyawan', 'fa fa-users', 1, 0, 1, 0, 3);
INSERT INTO `tb_page` VALUES (4, 'admin/c_role', 'Data Role', 'fa fa-lock', 1, 0, 1, 0, 6);
INSERT INTO `tb_page` VALUES (5, 'admin/c_user', 'Data User', 'fa fa-user', 1, 0, 1, 0, 7);
INSERT INTO `tb_page` VALUES (30, 'admin/Lpd', 'Data LPD', 'fa fa-file', 2, 0, 1, 0, 13);
INSERT INTO `tb_page` VALUES (34, 'admin/Lpd/Report', 'Laporan LPD', 'fa fa-list', 3, 0, 1, 0, 4);
INSERT INTO `tb_page` VALUES (6, 'admin/MemoDinas', 'Data Memo Dinas', 'fa fa-file', 2, 0, 1, 0, 8);
INSERT INTO `tb_page` VALUES (31, 'admin/MemoDinas/Report', 'Laporan Memo Dinas', 'fa fa-list', 3, 0, 1, 0, 1);
INSERT INTO `tb_page` VALUES (25, 'admin/Sppd', 'Data SPPD', 'fa fa-file', 2, 0, 1, 0, 10);
INSERT INTO `tb_page` VALUES (29, 'admin/Sppd/Persetujuan', 'Persetujuan SPPD', 'fa fa-check', 2, 0, 1, 0, 12);
INSERT INTO `tb_page` VALUES (33, 'admin/Sppd/Report', 'Laporan SPPD', 'fa fa-list', 3, 0, 1, 0, 3);
INSERT INTO `tb_page` VALUES (7, 'admin/Spt', 'Data SPT', 'fa fa-file', 2, 0, 1, 0, 9);
INSERT INTO `tb_page` VALUES (26, 'admin/Spt/antrianSpt', 'Data SPT Saya', 'fa fa-file', 2, 0, 1, 0, 11);
INSERT INTO `tb_page` VALUES (32, 'admin/Spt/Report', 'Laporan SPT', 'fa fa-list', 3, 0, 1, 0, 2);

-- ----------------------------
-- Table structure for tb_page_category
-- ----------------------------
DROP TABLE IF EXISTS `tb_page_category`;
CREATE TABLE `tb_page_category`  (
  `category_id` int(3) NOT NULL AUTO_INCREMENT,
  `category_title` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `category_icon` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `category_sort` int(2) NULL DEFAULT NULL,
  PRIMARY KEY (`category_id`) USING BTREE,
  UNIQUE INDEX `a`(`category_id`) USING BTREE,
  INDEX `b`(`category_title`) USING BTREE,
  INDEX `c`(`category_icon`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_page_category
-- ----------------------------
INSERT INTO `tb_page_category` VALUES (1, 'Data Master', 'fa fa-indent', 1);
INSERT INTO `tb_page_category` VALUES (2, 'Data Transaksi', 'fa fa-indent', 2);
INSERT INTO `tb_page_category` VALUES (3, 'Data Laporan', 'fa fa-indent', 3);

-- ----------------------------
-- Table structure for tb_role
-- ----------------------------
DROP TABLE IF EXISTS `tb_role`;
CREATE TABLE `tb_role`  (
  `id_role` int(3) NOT NULL AUTO_INCREMENT,
  `nama_role` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_role`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_role
-- ----------------------------
INSERT INTO `tb_role` VALUES (1, 'Direktur');
INSERT INTO `tb_role` VALUES (2, 'Staff');
INSERT INTO `tb_role` VALUES (3, 'Keuangan');
INSERT INTO `tb_role` VALUES (4, 'Admin');
INSERT INTO `tb_role` VALUES (5, 'Manager');

-- ----------------------------
-- Table structure for tb_sppd
-- ----------------------------
DROP TABLE IF EXISTS `tb_sppd`;
CREATE TABLE `tb_sppd`  (
  `id_sppd` int(2) NOT NULL AUTO_INCREMENT,
  `no_surat` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `asal` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tujuan` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `lama_perjalanan` int(2) NULL DEFAULT NULL,
  `tanggal_berangkat` date NULL DEFAULT NULL,
  `tanggal_pulang` date NULL DEFAULT NULL,
  `created_datetime` datetime(0) NULL DEFAULT NULL,
  `sppd_jarak` int(2) NULL DEFAULT NULL,
  `transportasi` enum('kendaraan dinas','kendaraan pribadi','kendaraan umum') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `nik` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `no_spt` int(2) NULL DEFAULT NULL,
  `sppd_status_id` int(2) NULL DEFAULT NULL,
  PRIMARY KEY (`id_sppd`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_sppd
-- ----------------------------
INSERT INTO `tb_sppd` VALUES (1, 'SPPD/I/0001', 'Kota Binjai', 'Kota Pekalaongan', 4, '2020-01-01', '2020-01-04', '2020-01-15 11:06:00', NULL, 'kendaraan dinas', '201805230004', 2, 3);
INSERT INTO `tb_sppd` VALUES (2, 'SPPD/I/0001', 'Kota Depok', 'Kota Pekalongan', 4, '2020-01-01', '2020-01-04', '2020-01-15 22:36:29', NULL, 'kendaraan dinas', '201605160016', 2, 2);
INSERT INTO `tb_sppd` VALUES (3, 'SPPD/I/0001', 'Bogor', 'Depok', 4, '2020-01-01', '2020-01-04', '2020-01-16 06:13:53', NULL, 'kendaraan pribadi', '201603010014', 2, 2);
INSERT INTO `tb_sppd` VALUES (4, 'SPPD/I/0002', 'DKI Jakarta', 'Kota Semarang', 5, '2020-01-20', '2020-01-24', '2020-01-16 12:02:20', NULL, 'kendaraan pribadi', '201501090009', 3, 2);
INSERT INTO `tb_sppd` VALUES (5, 'nomor11111', 'jakarta', 'bogor', 2, '2020-02-27', '2020-02-28', '2020-02-17 10:29:53', 15, 'kendaraan pribadi', '201605160016', 4, 1);

-- ----------------------------
-- Table structure for tb_sppd_history
-- ----------------------------
DROP TABLE IF EXISTS `tb_sppd_history`;
CREATE TABLE `tb_sppd_history`  (
  `history_id` int(2) NOT NULL AUTO_INCREMENT,
  `history_datetime` datetime(0) NULL DEFAULT NULL,
  `history_keterangan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `sppd_status_id` int(2) NULL DEFAULT NULL,
  `created_by` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`history_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_sppd_history
-- ----------------------------
INSERT INTO `tb_sppd_history` VALUES (1, '2020-01-16 05:57:12', NULL, 2, '201002220002');
INSERT INTO `tb_sppd_history` VALUES (2, '2020-01-16 06:13:53', NULL, 1, '201603010014');
INSERT INTO `tb_sppd_history` VALUES (3, '2020-01-16 06:18:26', NULL, 2, '201405300005');
INSERT INTO `tb_sppd_history` VALUES (4, '2020-01-16 06:19:43', NULL, 2, '201405300005');
INSERT INTO `tb_sppd_history` VALUES (5, '2020-01-16 12:02:20', NULL, 1, '201501090009');
INSERT INTO `tb_sppd_history` VALUES (6, '2020-01-16 12:42:56', NULL, 2, '201512300013');
INSERT INTO `tb_sppd_history` VALUES (7, '2020-01-16 12:43:03', NULL, 3, '201512300013');
INSERT INTO `tb_sppd_history` VALUES (8, '2020-02-17 10:19:33', NULL, 1, '201605160016');

-- ----------------------------
-- Table structure for tb_sppd_status
-- ----------------------------
DROP TABLE IF EXISTS `tb_sppd_status`;
CREATE TABLE `tb_sppd_status`  (
  `sppd_status_id` int(2) NOT NULL AUTO_INCREMENT,
  `sppd_status_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `sppd_status_sort` int(2) NULL DEFAULT NULL,
  `sppd_status_color` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`sppd_status_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_sppd_status
-- ----------------------------
INSERT INTO `tb_sppd_status` VALUES (1, 'pengajuan', 1, 'label-primary');
INSERT INTO `tb_sppd_status` VALUES (2, 'disetujui manager', 2, 'label-success');
INSERT INTO `tb_sppd_status` VALUES (3, 'ditolak manager', 3, 'label-danger');

-- ----------------------------
-- Table structure for tb_spt
-- ----------------------------
DROP TABLE IF EXISTS `tb_spt`;
CREATE TABLE `tb_spt`  (
  `no_spt` int(2) NOT NULL AUTO_INCREMENT,
  `kegiatan` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `tanggal` date NULL DEFAULT NULL,
  `lama` int(255) NULL DEFAULT NULL,
  `tempat` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_by` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_datetime` datetime(0) NULL DEFAULT NULL,
  `id_memo` int(2) NULL DEFAULT NULL,
  PRIMARY KEY (`no_spt`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_spt
-- ----------------------------
INSERT INTO `tb_spt` VALUES (1, 'tes', '2020-01-28', 2, 'tes', '201805230004', '2020-01-14 06:10:41', 2);
INSERT INTO `tb_spt` VALUES (2, 'jalan jalan', '2020-01-30', 3, 'Depok', '201805230004', '2020-01-14 06:45:32', 2);
INSERT INTO `tb_spt` VALUES (3, 'Memperbaiki server', '2020-01-20', 2, 'DKI Jakarta', '200911060001', '2020-01-16 12:00:32', 4);
INSERT INTO `tb_spt` VALUES (4, 'training', '2020-02-27', 1, 'Jakarata', '200911060001', '2020-02-17 10:18:44', 7);

-- ----------------------------
-- Table structure for tb_spt_detail
-- ----------------------------
DROP TABLE IF EXISTS `tb_spt_detail`;
CREATE TABLE `tb_spt_detail`  (
  `nik` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `no_spt` int(2) NULL DEFAULT NULL,
  INDEX `no_spt`(`no_spt`) USING BTREE,
  CONSTRAINT `no_spt` FOREIGN KEY (`no_spt`) REFERENCES `tb_spt` (`no_spt`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_spt_detail
-- ----------------------------
INSERT INTO `tb_spt_detail` VALUES ('201105180003', 1);
INSERT INTO `tb_spt_detail` VALUES ('201105180003', 1);
INSERT INTO `tb_spt_detail` VALUES ('201509010010', 2);
INSERT INTO `tb_spt_detail` VALUES ('201603010014', 2);
INSERT INTO `tb_spt_detail` VALUES ('201605160016', 2);
INSERT INTO `tb_spt_detail` VALUES ('201808070022', 2);
INSERT INTO `tb_spt_detail` VALUES ('201501090009', 3);
INSERT INTO `tb_spt_detail` VALUES ('201509010010', 3);
INSERT INTO `tb_spt_detail` VALUES ('201605160016', 4);
INSERT INTO `tb_spt_detail` VALUES ('201408260007', 4);

-- ----------------------------
-- Table structure for tb_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user`  (
  `user_id` int(3) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  `password` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `id_role` int(3) NULL DEFAULT NULL,
  `nik` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`) USING BTREE,
  INDEX `nik`(`nik`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 50 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_user
-- ----------------------------
INSERT INTO `tb_user` VALUES (1, 'admin', 'a60bbb136e1986c3317e560df263dcf5bf009b348f5de34c4418ff4e32aeb3d6ddaee8586c7db20e40bcdfa20037789cb026e68dcc18d61d134a45cd3342d93cjavbl+ne4V9O4a+QrL6phh6GlZmGgvminbLmfSZJdCw=', 4, '201805230004');
INSERT INTO `tb_user` VALUES (32, 'nia', 'a60bbb136e1986c3317e560df263dcf5bf009b348f5de34c4418ff4e32aeb3d6ddaee8586c7db20e40bcdfa20037789cb026e68dcc18d61d134a45cd3342d93cjavbl+ne4V9O4a+QrL6phh6GlZmGgvminbLmfSZJdCw=', 1, '200911060001');
INSERT INTO `tb_user` VALUES (33, 'marcel yu', 'e1ab72d478b8ad4fb1e7c8c47d8b201b092965313f54407f4c2930a1ae9abf1c0ddcba7aaef5429d6999a49dff7218678cce8ac7829ffd7261e3a775f37fae14IjGPzBqsJ2FPrzq4HT1jiNUqP68WqcPL7ein6rDmS3I=', 5, '201002220002');
INSERT INTO `tb_user` VALUES (34, 'umayah', 'a60bbb136e1986c3317e560df263dcf5bf009b348f5de34c4418ff4e32aeb3d6ddaee8586c7db20e40bcdfa20037789cb026e68dcc18d61d134a45cd3342d93cjavbl+ne4V9O4a+QrL6phh6GlZmGgvminbLmfSZJdCw=', 2, '201105180003');
INSERT INTO `tb_user` VALUES (35, 'ronni', 'a60bbb136e1986c3317e560df263dcf5bf009b348f5de34c4418ff4e32aeb3d6ddaee8586c7db20e40bcdfa20037789cb026e68dcc18d61d134a45cd3342d93cjavbl+ne4V9O4a+QrL6phh6GlZmGgvminbLmfSZJdCw=', 3, '201208010004');
INSERT INTO `tb_user` VALUES (36, 'alein', 'a60bbb136e1986c3317e560df263dcf5bf009b348f5de34c4418ff4e32aeb3d6ddaee8586c7db20e40bcdfa20037789cb026e68dcc18d61d134a45cd3342d93cjavbl+ne4V9O4a+QrL6phh6GlZmGgvminbLmfSZJdCw=', 5, '201405300005');
INSERT INTO `tb_user` VALUES (37, 'witri', 'a60bbb136e1986c3317e560df263dcf5bf009b348f5de34c4418ff4e32aeb3d6ddaee8586c7db20e40bcdfa20037789cb026e68dcc18d61d134a45cd3342d93cjavbl+ne4V9O4a+QrL6phh6GlZmGgvminbLmfSZJdCw=', 5, '201408260006');
INSERT INTO `tb_user` VALUES (38, 'evi', 'a60bbb136e1986c3317e560df263dcf5bf009b348f5de34c4418ff4e32aeb3d6ddaee8586c7db20e40bcdfa20037789cb026e68dcc18d61d134a45cd3342d93cjavbl+ne4V9O4a+QrL6phh6GlZmGgvminbLmfSZJdCw=', 2, '201501090009');
INSERT INTO `tb_user` VALUES (39, 'riska', 'a60bbb136e1986c3317e560df263dcf5bf009b348f5de34c4418ff4e32aeb3d6ddaee8586c7db20e40bcdfa20037789cb026e68dcc18d61d134a45cd3342d93cjavbl+ne4V9O4a+QrL6phh6GlZmGgvminbLmfSZJdCw=', 2, '201506250008');
INSERT INTO `tb_user` VALUES (40, 'yuyun', 'a60bbb136e1986c3317e560df263dcf5bf009b348f5de34c4418ff4e32aeb3d6ddaee8586c7db20e40bcdfa20037789cb026e68dcc18d61d134a45cd3342d93cjavbl+ne4V9O4a+QrL6phh6GlZmGgvminbLmfSZJdCw=', 2, '201509010010');
INSERT INTO `tb_user` VALUES (41, 'sakti', 'a60bbb136e1986c3317e560df263dcf5bf009b348f5de34c4418ff4e32aeb3d6ddaee8586c7db20e40bcdfa20037789cb026e68dcc18d61d134a45cd3342d93cjavbl+ne4V9O4a+QrL6phh6GlZmGgvminbLmfSZJdCw=', 2, '201510230011');
INSERT INTO `tb_user` VALUES (42, 'wanda', 'a60bbb136e1986c3317e560df263dcf5bf009b348f5de34c4418ff4e32aeb3d6ddaee8586c7db20e40bcdfa20037789cb026e68dcc18d61d134a45cd3342d93cjavbl+ne4V9O4a+QrL6phh6GlZmGgvminbLmfSZJdCw=', 2, '201511260012');
INSERT INTO `tb_user` VALUES (43, 'aldo', 'a60bbb136e1986c3317e560df263dcf5bf009b348f5de34c4418ff4e32aeb3d6ddaee8586c7db20e40bcdfa20037789cb026e68dcc18d61d134a45cd3342d93cjavbl+ne4V9O4a+QrL6phh6GlZmGgvminbLmfSZJdCw=', 5, '201512300013');
INSERT INTO `tb_user` VALUES (44, 'gunawan', 'a60bbb136e1986c3317e560df263dcf5bf009b348f5de34c4418ff4e32aeb3d6ddaee8586c7db20e40bcdfa20037789cb026e68dcc18d61d134a45cd3342d93cjavbl+ne4V9O4a+QrL6phh6GlZmGgvminbLmfSZJdCw=', 2, '201603010014');
INSERT INTO `tb_user` VALUES (45, 'cahyo', 'a60bbb136e1986c3317e560df263dcf5bf009b348f5de34c4418ff4e32aeb3d6ddaee8586c7db20e40bcdfa20037789cb026e68dcc18d61d134a45cd3342d93cjavbl+ne4V9O4a+QrL6phh6GlZmGgvminbLmfSZJdCw=', 2, '201605160015');
INSERT INTO `tb_user` VALUES (46, 'tria', 'a60bbb136e1986c3317e560df263dcf5bf009b348f5de34c4418ff4e32aeb3d6ddaee8586c7db20e40bcdfa20037789cb026e68dcc18d61d134a45cd3342d93cjavbl+ne4V9O4a+QrL6phh6GlZmGgvminbLmfSZJdCw=', 2, '201605160016');
INSERT INTO `tb_user` VALUES (47, 'walidin', 'a60bbb136e1986c3317e560df263dcf5bf009b348f5de34c4418ff4e32aeb3d6ddaee8586c7db20e40bcdfa20037789cb026e68dcc18d61d134a45cd3342d93cjavbl+ne4V9O4a+QrL6phh6GlZmGgvminbLmfSZJdCw=', 2, '201610260017');
INSERT INTO `tb_user` VALUES (48, 'heru', 'a60bbb136e1986c3317e560df263dcf5bf009b348f5de34c4418ff4e32aeb3d6ddaee8586c7db20e40bcdfa20037789cb026e68dcc18d61d134a45cd3342d93cjavbl+ne4V9O4a+QrL6phh6GlZmGgvminbLmfSZJdCw=', 2, '201705260018');
INSERT INTO `tb_user` VALUES (49, 'via', 'a60bbb136e1986c3317e560df263dcf5bf009b348f5de34c4418ff4e32aeb3d6ddaee8586c7db20e40bcdfa20037789cb026e68dcc18d61d134a45cd3342d93cjavbl+ne4V9O4a+QrL6phh6GlZmGgvminbLmfSZJdCw=', 2, '201801030019');

-- ----------------------------
-- Table structure for tb_user_role
-- ----------------------------
DROP TABLE IF EXISTS `tb_user_role`;
CREATE TABLE `tb_user_role`  (
  `id_user_role` int(3) NOT NULL AUTO_INCREMENT,
  `page_id` int(3) NULL DEFAULT NULL,
  `id_role` int(3) NULL DEFAULT NULL,
  PRIMARY KEY (`id_user_role`) USING BTREE,
  INDEX `page_id`(`page_id`) USING BTREE,
  INDEX `id_role`(`id_role`) USING BTREE,
  CONSTRAINT `tb_user_role_ibfk_1` FOREIGN KEY (`id_role`) REFERENCES `tb_role` (`id_role`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 93 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_user_role
-- ----------------------------
INSERT INTO `tb_user_role` VALUES (38, 25, 2);
INSERT INTO `tb_user_role` VALUES (39, 26, 2);
INSERT INTO `tb_user_role` VALUES (61, 26, 5);
INSERT INTO `tb_user_role` VALUES (62, 29, 5);
INSERT INTO `tb_user_role` VALUES (63, 6, 1);
INSERT INTO `tb_user_role` VALUES (64, 7, 1);
INSERT INTO `tb_user_role` VALUES (77, 2, 4);
INSERT INTO `tb_user_role` VALUES (78, 3, 4);
INSERT INTO `tb_user_role` VALUES (79, 1, 4);
INSERT INTO `tb_user_role` VALUES (80, 4, 4);
INSERT INTO `tb_user_role` VALUES (81, 5, 4);
INSERT INTO `tb_user_role` VALUES (82, 28, 4);
INSERT INTO `tb_user_role` VALUES (83, 6, 4);
INSERT INTO `tb_user_role` VALUES (84, 7, 4);
INSERT INTO `tb_user_role` VALUES (85, 25, 4);
INSERT INTO `tb_user_role` VALUES (86, 26, 4);
INSERT INTO `tb_user_role` VALUES (87, 29, 4);
INSERT INTO `tb_user_role` VALUES (88, 30, 4);
INSERT INTO `tb_user_role` VALUES (89, 31, 4);
INSERT INTO `tb_user_role` VALUES (90, 32, 4);
INSERT INTO `tb_user_role` VALUES (91, 33, 4);
INSERT INTO `tb_user_role` VALUES (92, 34, 4);

SET FOREIGN_KEY_CHECKS = 1;
