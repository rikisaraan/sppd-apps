<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_Sppd extends CI_Model {
    public $createBy="";
    public $now="";

    public function __construct(){ 
        parent::__construct();
        $this->load->model('M_getPropertyTable', 'property');
        $this->load->model('M_karyawan', 'Karyawan');
        $this->load->model('M_divisi', 'divisi');
        $this->load->model('M_MemoDinas', 'MemoDinas');

        $this->createBy = $this->session->userdata("nik");    
        $this->now=date("Y-m-d H:i:s");
    }

    public function getStatusSppd()
    {
        $query = $this->db->get("sppd_status")->result();
        return $query;
    }

    public function getSppdRow($id_sppd){
                $this->db->select("sppd.*,karyawan.nama_karyawan,divisi.nama_divisi,jabatan.nama_jabatan,anggaran.*,sppd_status.sppd_status_name,sppd_status.sppd_status_color");  
                $this->db->join("karyawan","karyawan.nik=sppd.nik","left");
                $this->db->join("divisi","divisi.id_divisi=karyawan.id_divisi","left");
                $this->db->join("jabatan","jabatan.id_jabatan=karyawan.id_jabatan","left");
                $this->db->join("anggaran","anggaran.id_jabatan=jabatan.id_jabatan","left");
                $this->db->join("sppd_status","sppd_status.sppd_status_id=sppd.sppd_status_id","left");
        $query =$this->db->get_where('sppd',array("id_sppd"=>$id_sppd))->row();
        return $query;
    }
    
    public function getSppdAll($where=""){
        $this->db->join("karyawan","karyawan.nik=sppd.nik","left");
        $this->db->join("divisi","divisi.id_divisi=karyawan.id_divisi","left");
        $this->db->join("jabatan","jabatan.id_jabatan=karyawan.id_jabatan","left");
        $this->db->join("anggaran","anggaran.id_jabatan=jabatan.id_jabatan","left");
        $this->db->join("sppd_status","sppd_status.sppd_status_id=sppd.sppd_status_id","left");
        if($where==""){
            $query =$this->db->get('sppd');
        }else{
            $query =$this->db->get_where('sppd',$where);    
        }
        
        return $query;
    }

    public function add($no_spt=0,$id_sppd=0){        
        if($_POST){

            $no_surat = $this->input->post("no_surat");
            $asal = $this->input->post("asal");
            $tujuan = $this->input->post("tujuan");
            $tanggal_berangkat = $this->input->post("tanggal_berangkat");
            $tanggal_pulang = $this->input->post("tanggal_pulang");
            $status = "pengajuan";
            $transportasi = $this->input->post("transportasi");
            $sppd_jarak = $this->input->post("sppd_jarak");
            $sppd_status_id=1; //pengajuan
            
            $datetime1 = new DateTime($tanggal_berangkat);
		    $datetime2 = new DateTime($tanggal_pulang);
		    $difference = $datetime1->diff($datetime2);
            $lama_perjalanan=$difference->days+1;
            
            $filedPost=array(   
                                "no_surat"=>$no_surat,
                                "asal"=>$asal,
                                "tujuan"=>$tujuan,
                                "lama_perjalanan"=>$lama_perjalanan,
                                "tanggal_berangkat"=>$tanggal_berangkat,
                                "tanggal_pulang"=>$tanggal_pulang,
                                "sppd_status_id"=>$sppd_status_id,
                                "transportasi"=>$transportasi,
                                "sppd_jarak"=>$sppd_jarak,
                                "nik"=>$this->createBy,
                                "no_spt"=>$no_spt,                
                                "created_datetime"=>$this->now,
                            );
                    
            $filedPostHistory=array(
                "history_datetime"=>date("Y-m-d H:i:s"),
                "sppd_status_id"=>$sppd_status_id,
                "created_by"=>$this->createBy,
            );

            if($id_sppd){
                $this->db->update('sppd',$filedPost,array("id_sppd"=>$id_sppd));                
                $pesan="Data Berhasil Diubah";
            }else{
                $cekSppd = $this->db->get_where("sppd",array("no_spt"=>$no_spt));
                if($cekSppd->num_rows() > 0){
                    $pesan="Data SPPD sudah ada dengan nomor SPT ".$no_spt;
                }else{
                    $this->db->insert('sppd',$filedPost);
                    $this->db->insert('sppd_history',$filedPostHistory);
    
                    $pesan="Data Berhasil Disimpan";
                }

               
            }
            echo '  <script>
                        alert("'.$pesan.'"); 
                        document.location="'.site_url('admin/Sppd').'";
                    </script>';                 
        }
        
        $data['formData']=$this->property->getProperty($id_sppd,"Sppd","id_sppd");    
        return $data;
            
    }    

    public function delete($id_sppd=''){
        $this->db->delete('Sppd',array('id_sppd'=>$id_sppd));
        redirect('admin/Sppd');
    }

    public function persetujuanSppd($id_sppd=0)
    {
        if($_POST){
            $id_sppd = $this->input->post("id_sppd");
            $sppd_status_id = $this->input->post("sppd_status_id");
            $history_keterangan = $this->input->post("history_keterangan");

            $filedPost=array("sppd_status_id"=>$sppd_status_id);
            $filedPostHistory=array(
                                    "history_datetime"=>date("Y-m-d H:i:s"),
                                    "history_keterangan"=>$history_keterangan,
                                    "sppd_status_id"=>$sppd_status_id,
                                    "created_by"=>$this->createBy,
                                );
            
            if($sppd_status_id==""){
                echo '  <script>
                            alert("Status SPPD harus dipilih"); 
                            document.location="'.site_url('admin/Sppd/Persetujuan').'";
                        </script>';  
            }else{
                $this->db->update('sppd',$filedPost,array("id_sppd"=>$id_sppd));
                
                $this->db->insert('sppd_history',$filedPostHistory);

                echo '  <script>
                            alert("Data SPPD berhasil diproses"); 
                            document.location="'.site_url('admin/Sppd/Persetujuan').'";
                        </script>';  
            }
             
        }

        $data["data"] = $this->getSppdRow($id_sppd);
        return $data;
    }

    public function getReportSppd(){
        if($_POST){
            $periode_awal = $this->input->post("periode_awal");
            $periode_akhir = $this->input->post("periode_akhir");
            $id_divisi = $this->input->post("id_divisi");
            $nik = $this->input->post("nik");
            $sppd_status_id = $this->input->post("sppd_status_id");
        
                                            
                            if($periode_awal!="" && $periode_akhir!=""){
                                $this->db->where("DATE_FORMAT(tb_spt.tanggal,'%Y-%m-%d') >=",$periode_awal);
                                $this->db->where("DATE_FORMAT(tb_spt.tanggal,'%Y-%m-%d') <=",$periode_akhir);
                            }
                            if ($id_divisi!="") {
                                $this->db->where("karyawan.id_divisi",$id_divisi);
                            }
                            if($nik!=""){
                                $this->db->where("sppd.nik",$nik);
                            }
                            if($sppd_status_id!=""){
                                $this->db->where("sppd.sppd_status_id",$sppd_status_id);
                            }
                            $this->db->select("sppd.*,karyawan.nama_karyawan,divisi.nama_divisi,jabatan.nama_jabatan,anggaran.*,sppd_status.sppd_status_name,sppd_status.sppd_status_color");  
                            $this->db->join("karyawan","karyawan.nik=sppd.nik","left");
                            $this->db->join("divisi","divisi.id_divisi=karyawan.id_divisi","left");
                            $this->db->join("jabatan","jabatan.id_jabatan=karyawan.id_jabatan","left");
                            $this->db->join("anggaran","anggaran.id_jabatan=jabatan.id_jabatan","left");
                            $this->db->join("sppd_status","sppd_status.sppd_status_id=sppd.sppd_status_id","left");
            $querySummary = $this->db->get("sppd");
            $data['querySummary']=$querySummary;
        }
        
        $data['dataDivisi']=$this->divisi->getDivisiAll();
        $data['dataKaryawan']=$this->Karyawan->getKaryawanAll();      
        $data['dataSppdStatus']=$this->getStatusSppd();      

        return $data;
    }
}
