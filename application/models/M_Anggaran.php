<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_Anggaran extends CI_Model {
	
    public function __construct(){ 
        parent::__construct();
		//untuk memanggil saat diedit pada textbox
        $this->load->model('M_getPropertyTable', 'property');
        $this->load->model('M_jabatan', 'jabatan');
    }    

    public function getAnggaranAll($where=""){
        $this->db->join("jabatan","jabatan.id_jabatan=anggaran.id_jabatan","left");
        if($where==""){
            $query =$this->db->get('anggaran');
        }else{
            $query =$this->db->get_where('anggaran',$where);    
        }                
        return $query;
    }

    public function add($id_anggaran=0){
        if($_POST){        
            
            
            $uang_makan = preg_replace("/\D/","", $this->input->post("uang_makan"));
            $uang_saku = preg_replace("/\D/","", $this->input->post("uang_saku"));
            $uang_penginapan = preg_replace("/\D/","", $this->input->post("uang_penginapan"));
            $uang_transport = preg_replace("/\D/","", $this->input->post("uang_transport"));
            $id_jabatan = $this->input->post("id_jabatan");

            
            $qCekSama = $this->db->get_where("anggaran",array("id_jabatan"=>$id_jabatan));
            
            $filedPost=array(
                                "uang_makan"=>$uang_makan,
                                "uang_saku"=>$uang_saku,
                                "uang_penginapan"=>$uang_penginapan,
                                "uang_transport"=>$uang_transport,
                                "id_jabatan"=>$id_jabatan,
                            );        
                            
            if($id_anggaran){            
                $this->db->update('anggaran',$filedPost,array("id_anggaran"=>$id_anggaran));
                $pesan="Data Berhasil Diubah";
            }else{
               
                if($qCekSama->num_rows() < 1){
                     $this->db->insert('anggaran',$filedPost);
                    $pesan="Data Berhasil Disimpan";     
                }else{
                    echo '  <script>
                                alert("Data Anggaran untuk jabatan ini sudah ada"); 
                                document.location="'.site_url('admin/Anggaran').'";
                            </script>';
                }  
            }
            
            echo '  <script>
                alert("'.$pesan.'"); 
                document.location="'.site_url('admin/Anggaran').'";
            </script>';
            
                    
        }
        
        $data['formData']=$this->property->getProperty($id_anggaran,"anggaran","id_anggaran");
        $data['dataJabatan']=$this->jabatan->getJabatanAll();

        return $data;
            
    }    

    public function delete($id_anggaran=''){
        $this->db->delete('anggaran',array('id_anggaran'=>$id_anggaran));
        redirect('admin/Anggaran');
    }

}
