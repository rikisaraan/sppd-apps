<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_divisi extends CI_Model {
	
    public function __construct(){ 
        parent::__construct();
		//untuk memanggil saat diedit pada textbox
        $this->load->model('M_getPropertyTable', 'property');
    }    

    public function getDivisiAll($where=""){
                
        if($where==""){
            $query =$this->db->get('divisi');
        }else{
            $query =$this->db->get_where('divisi',$where);    
        }                
        return $query;
    }

    public function add($id_divisi=0){
        if($_POST){                        
            $nama_divisi = $this->input->post("nama_divisi");
            $keterangan = $this->input->post("keterangan");
            
            $qCekSama = $this->db->get_where("divisi",array("nama_divisi"=>$nama_divisi))->num_rows();

            if($qCekSama < 1){
                $filedPost=array("nama_divisi"=>$nama_divisi,"keterangan"=>$keterangan,);
                if($id_divisi){
                    $this->db->update('divisi',$filedPost,array("id_divisi"=>$id_divisi));
					$pesan="Data Berhasil Diubah";
                }else{
                    $this->db->insert('divisi',$filedPost);
					$pesan="Data Berhasil Disimpan";
                }
				echo '  <script>
					alert("'.$pesan.'"); 
					document.location="'.site_url('admin/c_divisi').'";
				</script>';
            }else{
                echo '  <script>
                            alert("Data yang anda masukan sudah ada"); 
                            document.location="'.site_url('admin/c_divisi').'";
                        </script>';
            }          
        }
        
        $data['formData']=$this->property->getProperty($id_divisi,"divisi","id_divisi");
        
        return $data;
            
    }    

    public function delete($id_divisi=''){
        $this->db->delete('divisi',array('id_divisi'=>$id_divisi));
        redirect('admin/c_divisi');
    }

}
