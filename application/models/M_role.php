<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_role extends CI_Model {
	
    public function __construct(){ 
        parent::__construct();
        $this->load->model('M_getPropertyTable', 'property');
    }    
    
    public function getroleAll(){                            
        $query =$this->db->get('role');
        return $query;
    }

    public function add($id_role=0){
        if($_POST){                        
            $nama_role = $this->input->post("nama_role");
            
            $qCekSama = $this->db->get_where("role",array("nama_role"=>$nama_role))->num_rows();                
            if($qCekSama < 1){
                $filedPost=array("nama_role"=>$nama_role);
                if($id_role){
                    $this->db->update('role',$filedPost,array("id_role"=>$id_role));
					echo '  <script>
					alert("Data Berhasil Diubah"); 
					document.location="'.site_url('admin/c_role').'";
					</script>';
                }else{
                    $this->db->insert('role',$filedPost);
 					echo '  <script>
					alert("Data Berhasil Disimpan"); 
					document.location="'.site_url('admin/c_role').'";
					</script>';
				}

                //redirect('admin/c_role');
            }else{
                echo '  <script>
                            alert("Data yang anda masukan sudah ada"); 
                            document.location="'.site_url('admin/c_role').'";
                        </script>';
            }            
        }
        
        $data['formData']=$this->property->getProperty($id_role,"role","id_role");
        
        return $data;
            
    }    

    public function delete($id_role=''){
        $this->db->delete('role',array('id_role'=>$id_role));
        redirect('admin/c_role');
    }

    public function setRole($id_role=''){        
        if(isset($_POST['page_id'])){
            $query = array();
            $query[] = "START TRANSACTION";
                
            $page_id = $this->input->post('page_id');

            $query[] = "DELETE FROM tb_user_role where id_role='{$id_role}'";
            foreach ($page_id as $value) {
                $query[]="INSERT INTO tb_user_role (id_role,page_id)values('{$id_role}','{$value}')";
            }

            $query[] = "COMMIT";

            foreach ($query as $q) {
                //print_r($q);
                $this->db->query($q);
            }
                    
            echo '  <script>
                    alert("Data berhasil disimpan"); 
                    document.location="'.site_url('admin/c_role').'";
                </script>';

        }

        $data["category_page"] = $this->db->get("page_category");
        return $data;
    }
}
