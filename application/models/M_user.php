<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_user extends CI_Model {
	
    public function __construct(){ 
        parent::__construct();
        $this->load->model('M_getPropertyTable', 'property');
        $this->load->model('M_role','role');        
		$this->load->library('encryption');    
    }    
    
    public function getuserAll(){                            
                $this->db->join("role","role.id_role=user.id_role","left");
                $this->db->join("karyawan","karyawan.nik=user.nik","left");
        $query =$this->db->get('user');
        return $query;
    }

    function getStaf($nama_karyawan=""){
                    
                $this->db->like('nama_karyawan',$nama_karyawan);
                $this->db->group_by("nik");        
        $query =$this->db->get('karyawan');
        return $query;
    }

    public function add($nik=0,$user_id=0){
        
        if($_POST){           
            $user_name = $this->input->post("user_name");
            $id_role = $this->input->post("id_role");
            $password = $this->encryption->encrypt($this->input->post("password"));
           
                $filedPost=array("nik"=>$nik,"user_name"=>$user_name,"id_role"=>$id_role,"password"=>$password,);
				//print_r($filedPost);exit;
				if ($user_id){
					$this->db->update('user',$filedPost,array("user_id"=>$user_id));
					echo '  <script>
                    alert("Data Berhasil Diubah"); 
					document.location="'.site_url('admin/c_user').'";
                    </script>';
				}else{
					$qCekSama = $this->db->get_where("user",array("user_name"=>$user_name))->num_rows();
				 if($qCekSama < 1){
					 $this->db->insert('user',$filedPost);
					 echo '  <script>
                    alert("Data Berhasil Disimpan"); 
					document.location="'.site_url('admin/c_karyawan').'";
                    </script>';
            }else{
                echo '  <script>
                            alert("Username yang anda masukan sudah ada"); 
                            document.location="'.site_url('admin/c_karyawan').'";
                        </script>';
            }  
			}			
                   
        }
        
        $data['formData']=$this->property->getProperty($user_id,"user","user_id");
        $data['dataRole']=$this->role->getroleAll();
        
        
        return $data;
            
    }    

    public function delete($id_user=''){
        $this->db->delete('user',array('user_id'=>$id_user));
        redirect('admin/c_user');
    }

    public function check_login($user_name,$password){
        
        $this->db->join('role', 'role.id_role = user.id_role', 'left');
        $this->db->join('karyawan', 'karyawan.nik = user.nik', 'left');
        $result = $this->db->get_where('user', array('user_name' => $user_name));
        
        if($result->num_rows() == 1):
            $data=$result->row();
            if($this->encryption->decrypt($data->password)==$password):                
                return $result->row();    
            endif;
        endif;
        
        return false;        
    }

    public function changePass(){
        $nik = $this->session->userdata("nik");

                        $this->db->join("karyawan","karyawan.nik=user.nik","left");
                        $this->db->join("divisi","divisi.id_divisi=karyawan.id_divisi","left");
                        $this->db->join("jabatan","jabatan.id_jabatan=karyawan.id_jabatan","left");
        $datakaryawan=  $this->db->get_where("user",array('user.nik'=>$nik))->row();
        //print_r($datakaryawan);exit;


        if($_POST){
            //print_r($_POST);exit;            
            
            $password_lama=$this->input->post("password_lama");
            $password_baru=$this->input->post("password_baru");
            $password_konfirmasi_baru=$this->input->post("password_konfirmasi_baru");

            if($this->encryption->decrypt($datakaryawan->password)==$password_lama){
                $postData=array("password"=>$this->encryption->encrypt($password_baru));            
                $this->db->update('user',$postData,array('nik'=>$nik));
                echo '  <script>
                            alert("Kata sandi berhasil diperbarui silahkan login kembali"); 
                            document.location="'.site_url('main/logout').'";
                        </script>';                
            }else{
                echo '  <script>
                            alert("Kata sandi lama anda salah"); 
                            document.location="'.site_url('main/dashboard').'";
                        </script>';
                        
            }
        }
        $pesan_error="";
        return '
            <form action="'.current_url().'" method="post">               
                <table class="table table-bordered">
                    <tbody>                     
                        <tr>
                            <td colspan="2">Apakah anda ingin merubah password ini ?</td>
                        </tr>
                        <tr width="15%">
                            <td>Kata Sandi Lama</td>
                            <td><input class="form-control" type="password" name="password_lama" id=""></td>
                        </tr>
                        <tr>
                            <td>Kata Sandi Baru</td>
                            <td><input class="form-control" type="password" name="password_baru" id="password_baru"></td>
                        </tr>
                        <tr>
                            <td>Konfirmasi Kata Sandi Baru</td>
                            <td><input class="form-control" type="password" name="password_konfirmasi_baru" id="password_konfirmasi_baru"></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <button id="proses" type="submit" class="btn btn-flat btn-primary ">Proses &nbsp;&nbsp;<span class="fa fa-arrow-circle-right"></span></button>
                            </td>
                        </tr>
                    </tbody>
                </table>                
            </form>
            <script>
                $(document).on("change","#password_baru",function(){
                    var length = $("#password_baru").val().length;
                    if(length<6){
                        alert("Kata sandi terlalu pendek, minimum 6 karakter");
                        $("#password_baru").focus();
                        $("#proses").hide(100);
                    }else{
                        $("#proses").show(100);
                    }
                });
                $(document).on("change","#password_konfirmasi_baru",function(){
                    var password_konfirmasi_baru = $("#password_konfirmasi_baru").val();
                    var password_baru = $("#password_baru").val();
                    if(password_konfirmasi_baru!=password_baru){
                        alert("Kata sandi Baru tidak sama dengan Ulangi Kata Sandi");
                        $("#password_konfirmasi_baru").focus();
                        $("#proses").hide(100);
                    }else{
                        $("#proses").show(100);
                    }
                });
            </script>
            ';
    }
}
