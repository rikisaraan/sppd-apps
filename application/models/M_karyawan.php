<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_karyawan extends CI_Model {
	
    public function __construct(){ 
        parent::__construct();
        
        $this->load->model('M_getPropertyTable', 'property');
        $this->load->model('M_divisi', 'divisi');
        $this->load->model('M_jabatan', 'jabatan');
    }    
    
    public function getKaryawanWithWhere($where=""){                            
                    $this->db->join("user","user.nik=karyawan.nik","left");
                    $this->db->join("jabatan","jabatan.id_jabatan=karyawan.id_jabatan","left");
                    $this->db->join("divisi","divisi.id_divisi=karyawan.id_divisi","left");
        $query =    $this->db->get_where('karyawan',$where);
        //print_r($this->db->last_query());exit;
        return $query;
    }

    public function getKaryawanAll(){        
                    $this->db->join("jabatan","jabatan.id_jabatan=karyawan.id_jabatan","left");
                    $this->db->join("divisi","divisi.id_divisi=karyawan.id_divisi","left");
        $query =    $this->db->get('karyawan');
        return $query;
    }

    public function createAutoNik(){                    
                    $this->db->limit(1);
                    $this->db->order_by("nik","desc");
        $result =   $this->db->query('SELECT MAX(RIGHT(nik, 4)) as max_id FROM tb_karyawan');        
        $data = $result->row();
        $id_max = $data->max_id;
        $sort_num = (int) substr($id_max, 1, 4);
        $sort_num++;
        $new_code = date("Ymd").sprintf("%04s", $sort_num);
        return $new_code;

    }

    public function add($id=0){
        if($_POST){            
            $nik = $this->input->post("nik");
            $nama_karyawan = $this->input->post("nama_karyawan");
            $tempat_lahir = $this->input->post("tempat_lahir");
            $tgl_lahir = $this->input->post("tgl_lahir");
            $alamat = $this->input->post("alamat");
            $no_telp =  str_replace(['(', '_', ')', '-'," "],'', $this->input->post("no_telp"));
            $no_telp_darurat = str_replace(['(', '_', ')', '-'," "],'', $this->input->post("no_telp_darurat"));
            $email = $this->input->post("email");
            $tgl_masuk_kerja = $this->input->post("tgl_masuk_kerja");
            $jenis_kelamin = $this->input->post("jenis_kelamin");
            $status_karyawan = $this->input->post("status_karyawan");
            $agama = $this->input->post("agama");
            $id_divisi = $this->input->post("id_divisi");
            $id_jabatan = $this->input->post("id_jabatan");

            $filedPost=array(
                                "nik"=>$nik,
                                "nama_karyawan"=>$nama_karyawan,
                                "tempat_lahir"=>$tempat_lahir,
                                "tgl_lahir"=>$tgl_lahir,
                                "alamat"=>$alamat,
                                "no_telp"=>$no_telp,
                                "no_telp_darurat"=>$no_telp_darurat,
                                "email"=>$email,
                                "tgl_masuk_kerja"=>$tgl_masuk_kerja,
                                "jenis_kelamin"=>$jenis_kelamin,
                                "status_karyawan"=>$status_karyawan,
                                "agama"=>$agama,
                                "id_divisi"=>$id_divisi,
                                "id_jabatan"=>$id_jabatan,
                            );
                        
            
            if($id){
                $this->db->update('karyawan',$filedPost,array("nik"=>$id));
				echo '  <script>
                            alert("Data Berhasil Diubah"); 
                            document.location="'.site_url('admin/c_karyawan').'";
                        </script>';
            }else{
                $this->db->insert('karyawan',$filedPost);
				echo '  <script>
                            alert("Data Berhasil Disimpan"); 
                            document.location="'.site_url('admin/c_karyawan').'";
                        </script>';
            }

        }
                        
        $data['dataDivisi']=$this->divisi->getDivisiAll();
        $data['dataJabatan']=$this->jabatan->getJabatanAll();
        $data['autoNik']=$this->createAutoNik();
        $data['formData']=$this->property->getProperty($id,"karyawan","nik");
        
        return $data;
            
    }    

    public function delete($nik=''){
        $this->db->delete('karyawan',array('nik'=>$nik));
        redirect('admin/c_karyawan');
    }
}
