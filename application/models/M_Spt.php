<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_Spt extends CI_Model {
    public $sessId="";
    public $now="";

    public function __construct(){ 
        parent::__construct();
        $this->load->model('M_getPropertyTable', 'property');
        $this->load->model('M_karyawan', 'Karyawan');
        $this->load->model('M_divisi', 'divisi');
        $this->load->model('M_MemoDinas', 'MemoDinas');

        $this->sessId = $this->session->userdata("nik");    
        $this->now=date("Y-m-d H:i:s");
    }

    public function getSptRow($no_spt){
                $this->db->select("spt.*,memo_dinas.nomor_memo,memo_dinas.pengirim");
                $this->db->join("memo_dinas","memo_dinas.id_memo=spt.id_memo","left");
        $query =$this->db->get_where('spt',array("no_spt"=>$no_spt))->row();
        return $query;
    }
    
    public function getDetailSpt($no_spt){
                $this->db->join("karyawan","karyawan.nik=spt_detail.nik","left");
                $this->db->join("jabatan","jabatan.id_jabatan=karyawan.id_jabatan","left");
                $this->db->join("divisi","divisi.id_divisi=karyawan.id_divisi","left");
        $query =$this->db->get_where('spt_detail',array("no_spt"=>$no_spt))->result();
        return $query;
    }

    public function getSptByNik(){
                $this->db->select("spt.*,memo_dinas.nomor_memo,memo_dinas.pengirim,karyawan.nama_karyawan,spt_detail.nik,jabatan.nama_jabatan,divisi.nama_divisi");
                $this->db->join("karyawan","karyawan.nik=spt_detail.nik","left");
                $this->db->join("jabatan","jabatan.id_jabatan=karyawan.id_jabatan","left");
                $this->db->join("divisi","divisi.id_divisi=karyawan.id_divisi","left");
                $this->db->join("spt","spt.no_spt=spt_detail.no_spt","left");
                $this->db->join("memo_dinas","memo_dinas.id_memo=spt.id_memo","left");
        $query =$this->db->get_where('spt_detail',array("spt_detail.nik"=>$this->sessId))->result();
        return $query;
    }

    public function getSptAll($where=""){
        $this->db->join("memo_dinas","memo_dinas.id_memo=spt.id_memo","left");
        if($where==""){
            $query =$this->db->get('spt');
        }else{
            $query =$this->db->get_where('spt',$where);    
        }             
    
        return $query;
    }

    public function add($no_spt=0){        
        if($_POST){
            
            $kegiatan = $this->input->post("kegiatan");
            $tanggal = $this->input->post("tanggal");
            $lama = $this->input->post("lama");
            $tempat = $this->input->post("tempat");
            $id_memo = $this->input->post("id_memo");
            $nik = $this->input->post("nik");
            
            $filedPost=array(   "kegiatan"=>$kegiatan,
                                "tanggal"=>$tanggal,
                                "lama"=>$lama,
                                "tempat"=>$tempat,
                                "created_by"=>$this->sessId,
                                "created_datetime"=>$this->now,
                                "id_memo"=>$id_memo,                                
                            );
                    
            if($no_spt){
                $this->db->update('spt',$filedPost,array("no_spt"=>$no_spt));
                foreach($nik as $id){
                    $filedPostDetail=array("no_spt"=>$no_spt,"nik"=>$id); 
                    $this->db->delete("spt_detail",array("no_spt"=>$no_spt));
                    $this->db->insert('spt_detail',$filedPostDetail);
                }
                $pesan="Data Berhasil Diubah";
            }else{
                $this->db->insert('spt',$filedPost);
                $no_spt = $this->db->insert_id();                   
                foreach($nik as $id){
                    $filedPostDetail=array("no_spt"=>$no_spt,"nik"=>$id); 
                    $this->db->insert('spt_detail',$filedPostDetail);
                }

                $pesan="Data Berhasil Disimpan";
            }
            echo '  <script>
                        alert("'.$pesan.'"); 
                        document.location="'.site_url('admin/Spt').'";
                    </script>';                 
        }
        
        $data['formData']=$this->property->getProperty($no_spt,"spt","no_spt");
        $data['dataMemoDinas']=$this->MemoDinas->getMemoDinasAll();
        $data['dataKaryawan']=$this->Karyawan->getKaryawanAll();
        
        return $data;
            
    }    

    public function delete($no_spt=''){
        $this->db->delete('spt',array('no_spt'=>$no_spt));
        redirect('admin/Spt');
    }

    public function getReportSpt(){
        if($_POST){
            $periode_awal = $this->input->post("periode_awal");
            $periode_akhir = $this->input->post("periode_akhir");
            $id_divisi = $this->input->post("id_divisi");
            $nik = $this->input->post("nik");
        
                                            
                            if($periode_awal!="" && $periode_akhir!=""){
                                $this->db->where("DATE_FORMAT(tb_spt.tanggal,'%Y-%m-%d') >=",$periode_awal);
                                $this->db->where("DATE_FORMAT(tb_spt.tanggal,'%Y-%m-%d') <=",$periode_akhir);
                            }
                            if ($id_divisi!="") {
                                $this->db->where("karyawan.id_divisi",$id_divisi);
                            }
                            if($nik!=""){
                                $this->db->where("spt_detail.nik",$nik);
                            }
                            $this->db->select("spt.*,memo_dinas.nomor_memo,memo_dinas.pengirim,karyawan.nama_karyawan,spt_detail.nik,jabatan.nama_jabatan,divisi.nama_divisi");
                            $this->db->join("karyawan","karyawan.nik=spt_detail.nik","left");
                            $this->db->join("jabatan","jabatan.id_jabatan=karyawan.id_jabatan","left");
                            $this->db->join("divisi","divisi.id_divisi=karyawan.id_divisi","left");
                            $this->db->join("spt","spt.no_spt=spt_detail.no_spt","left");
                            $this->db->join("memo_dinas","memo_dinas.id_memo=spt.id_memo","left");
            $querySummary = $this->db->get("spt_detail");
            $data['querySummary']=$querySummary;
        }
        
        $data['dataDivisi']=$this->divisi->getDivisiAll();
        $data['dataKaryawan']=$this->Karyawan->getKaryawanAll();      

        return $data;
    }

}
