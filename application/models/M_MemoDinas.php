<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_MemoDinas extends CI_Model {
	
    public function __construct(){ 
        parent::__construct();
        $this->load->model('M_getPropertyTable', 'property');        
        $this->load->model('M_UploadFile', 'UploadFile');

        $this->sessId = $this->session->userdata("nik");
        $this->now=date("Y-m-d H:i:s");
    }

    public function getMemoDinasRow($id_memo){
        $query =$this->db->get_where('memo_dinas',array("id_memo"=>$id_memo))->row();
        return $query;
    }

    public function getMemoDinasAll($where=""){
                
        if($where==""){
            $query =$this->db->get('memo_dinas');
        }else{
            $query =$this->db->get_where('memo_dinas',$where);    
        }             
    
        return $query;
    }

    public function add($id_memo=0){
        if($_POST){
            $pengirim = $this->input->post("pengirim");
            $nomor_memo = $this->input->post("nomor_memo");
            $tanggal = $this->input->post("tanggal");
            $namafield="lampiran";
            $namaFolder="fileMemoDinas";

            //print_r($_FILES['lampiran']['name']);exit;

            if($_FILES['lampiran']['name']!=""){
                $cekUpload = $this->UploadFile->_uploadFile("memo_".$nomor_memo,$namafield,$namaFolder);
            
                $resultUpload = $cekUpload["kode"];
                $resultMessage = $cekUpload["message"];
                $fileName = $cekUpload["file_name"];

                if($resultUpload==200){
                    $filedPost=array(   "pengirim"=>$pengirim,
                                        "nomor_memo"=>$nomor_memo,
                                        "tanggal"=>$tanggal,
                                        "lampiran"=>$fileName,
                                        "created_by"=>$this->sessId,
                                        "created_datetime"=>$this->now
                                    );
                    if($id_memo){
                        $this->db->update('memo_dinas',$filedPost,array("id_memo"=>$id_memo));
                        $pesan="Data Berhasil Diubah";
                    }else{
                        $this->db->insert('memo_dinas',$filedPost);
                        $pesan="Data Berhasil Disimpan";
                    }
                    echo '  <script>
                                alert("'.$pesan.'"); 
                                document.location="'.site_url('admin/MemoDinas').'";
                            </script>';
                }
                else{
                    echo '  <script>
                                alert("'.$resultMessage.'"); 
                                document.location="'.site_url('admin/MemoDinas').'";
                            </script>';
                }   
            }else{
                $filedPost=array(   "pengirim"=>$pengirim,
                                    "nomor_memo"=>$nomor_memo,
                                    "tanggal"=>$tanggal,
                                    "created_by"=>$this->sessId,
                                    "created_datetime"=>$this->now
                                );
                if($id_memo){
                        $this->db->update('memo_dinas',$filedPost,array("id_memo"=>$id_memo));
                        $pesan="Data Berhasil Diubah";
                    }else{
                        $this->db->insert('memo_dinas',$filedPost);
                        $pesan="Data Berhasil Disimpan";
                    }
                    echo '  <script>
                                alert("'.$pesan.'"); 
                                document.location="'.site_url('admin/MemoDinas').'";
                            </script>';
            }
                            
        }
        
        $data['formData']=$this->property->getProperty($id_memo,"memo_dinas","id_memo");
        
        return $data;
            
    }    

    public function delete($id_memo=''){
        $this->db->delete('memo_dinas',array('id_memo'=>$id_memo));
        redirect('admin/MemoDinas');
    }


    public function getReportMemoDinas(){
        if($_POST){
            $periode_awal = $this->input->post("periode_awal");
            $periode_akhir = $this->input->post("periode_akhir");
        
                                            
                            if($periode_awal!="" && $periode_akhir!=""){
                                $this->db->where("DATE_FORMAT(tanggal,'%Y-%m-%d') >=",$periode_awal);
                                $this->db->where("DATE_FORMAT(tanggal,'%Y-%m-%d') <=",$periode_akhir);
                            }                            
            $querySummary = $this->db->get("memo_dinas");
            $data['querySummary']=$querySummary;
        }
        
        return $data;
    }

}
