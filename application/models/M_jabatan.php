<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_jabatan extends CI_Model {
	
    public function __construct(){ 
        parent::__construct();
        $this->load->model('M_getPropertyTable', 'property');
    }    
    
    public function getJabatanAll(){                            
        $query =    $this->db->get('jabatan');
        return $query;
    }

    public function add($id_jabatan=0){
        if($_POST){                        
            $nama_jabatan = $this->input->post("nama_jabatan");
            $keterangan = $this->input->post("keterangan");
            
            $qCekSama = $this->db->get_where("jabatan",array("nama_jabatan"=>$nama_jabatan))->num_rows();
            
            if($qCekSama < 1){
                $filedPost=array("nama_jabatan"=>$nama_jabatan,"keterangan"=>$keterangan,);
                if($id_jabatan){
                    $this->db->update('jabatan',$filedPost,array("id_jabatan"=>$id_jabatan));
					$pesan="Data Berhasil Diubah";
                }else{
                    $this->db->insert('jabatan',$filedPost);
					$pesan="Data Berhasil Disimpan";
                }
				echo '  <script>
					alert("'.$pesan.'"); 
					document.location="'.site_url('admin/c_jabatan').'";
				</script>';
            }else{
                echo '  <script>
                            alert("Data yang anda masukan sudah ada"); 
                            document.location="'.site_url('admin/c_jabatan').'";
                        </script>';
            }
        }
        $data['formData']=$this->property->getProperty($id_jabatan,"jabatan","id_jabatan");
        
        return $data;
            
    }    

    public function delete($id_jabatan=''){
        $this->db->delete('jabatan',array('id_jabatan'=>$id_jabatan));
        redirect('admin/c_jabatan');
    }
}
