<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_Lpd extends CI_Model {
	
    public function __construct(){ 
        parent::__construct();
        $this->load->model('M_getPropertyTable', 'property');
        $this->load->model('M_UploadFile', 'UploadFile');
        $this->load->model('M_karyawan', 'Karyawan');
        $this->load->model('M_divisi', 'divisi');
        
        $this->createBy = $this->session->userdata("nik");    
        $this->now=date("Y-m-d H:i:s");
    }

    public function getLpdRow($id_lpd){
        $query =$this->db->get_where('lpd',array("id_lpd"=>$id_lpd))->row();
        return $query;
    }

    public function getLpdAll($where=""){
                
        if($where==""){
            $query =$this->db->get('lpd');
        }else{
            $query =$this->db->get_where('lpd',$where);    
        }             
    
        return $query;
    }

    public function add($id_lpd=0){
        if($_POST){
            $kegiatan = $this->input->post("kegiatan");
            $tempat = $this->input->post("tempat");
            $tanggal_kegiatan = $this->input->post("tanggal_kegiatan");
            $laporan = $this->input->post("laporan");
            $keterangan = $this->input->post("keterangan");
            $klaim = $this->input->post("klaim");
            $nik = $this->createBy;

            $namafield="klaim";
            $namaFolder="fileLpd";
            $namaFile="lpd_".strtotime(date("Y-m-d H:i:s"));
                    
            $cekUpload = $this->UploadFile->_uploadFile($namaFile,$namafield,$namaFolder);
            
            if($_FILES['klaim']['name']==""){
                echo '  <script>
                            alert("Klaim Kwitansi tidak boleh kosong"); 
                            document.location="'.site_url('admin/Lpd').'";
                        </script>';
            }else{
                $resultUpload = $cekUpload["kode"];
                $resultMessage = $cekUpload["message"];
                $fileName = $cekUpload["file_name"];

                if($resultUpload==200){
                    $filedPost=array(   
                                        "kegiatan"=>$kegiatan,
                                        "tempat"=>$tempat,
                                        "tanggal_kegiatan"=>$tanggal_kegiatan,
                                        "laporan"=>$laporan,
                                        "keterangan"=>$keterangan,
                                        "klaim"=>$fileName,
                                        "nik"=>$nik,
                                    );
                    if($id_lpd){
                        $this->db->update('lpd',$filedPost,array("id_lpd"=>$id_lpd));
                        $pesan="Data Berhasil Diubah";
                    }else{
                        $this->db->insert('lpd',$filedPost);
                        $pesan="Data Berhasil Disimpan";
                    }
                    echo '  <script>
                                alert("'.$pesan.'"); 
                                document.location="'.site_url('admin/Lpd').'";
                            </script>';
                }
                else{
                    echo '  <script>
                                alert("'.$resultMessage.'"); 
                                document.location="'.site_url('admin/Lpd').'";
                            </script>';
                }
                
            }                    
        }
        
        $data['formData']=$this->property->getProperty($id_lpd,"lpd","id_lpd");
        
        return $data;
            
    }    

    public function delete($id_lpd=''){
        $this->db->delete('lpd',array('id_lpd'=>$id_lpd));
        redirect('admin/Lpd');
    }

    public function getReportLpd(){
        if($_POST){
            $periode_awal = $this->input->post("periode_awal");
            $periode_akhir = $this->input->post("periode_akhir");
            $id_divisi = $this->input->post("id_divisi");
            $nik = $this->input->post("nik");
                                                
                            if($periode_awal!="" && $periode_akhir!=""){
                                $this->db->where("DATE_FORMAT(tb_spt.tanggal_kegiatan,'%Y-%m-%d') >=",$periode_awal);
                                $this->db->where("DATE_FORMAT(tb_spt.tanggal_kegiatan,'%Y-%m-%d') <=",$periode_akhir);
                            }
                            if ($id_divisi!="") {
                                $this->db->where("karyawan.id_divisi",$id_divisi);
                            }
                            if($nik!=""){
                                $this->db->where("lpd.nik",$nik);
                            }
                            if($lpd_status_id!=""){
                                $this->db->where("lpd.lpd_status_id",$lpd_status_id);
                            }
                            $this->db->select("lpd.*,karyawan.nama_karyawan,divisi.nama_divisi,jabatan.nama_jabatan");  
                            $this->db->join("karyawan","karyawan.nik=lpd.nik","left");
                            $this->db->join("divisi","divisi.id_divisi=karyawan.id_divisi","left");
                            $this->db->join("jabatan","jabatan.id_jabatan=karyawan.id_jabatan","left");
            $querySummary = $this->db->get("lpd");
            $data['querySummary']=$querySummary;
        }
        
        $data['dataDivisi']=$this->divisi->getDivisiAll();
        $data['dataKaryawan']=$this->Karyawan->getKaryawanAll();                

        return $data;
    }
}
