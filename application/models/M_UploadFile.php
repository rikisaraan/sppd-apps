<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_UploadFile extends CI_Model {
	
    public function __construct(){ 
        parent::__construct();        
    }  
    public function _uploadFile($fileName,$namafield,$namaFolder){
        
        $config['upload_path']          = './public/uploads/'.$namaFolder.'/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['file_name']            = $fileName;
        $config['overwrite']			= true;
        $config['max_size']             = 1024*8; // 1MB

        $this->load->library('upload', $config);

        if ($this->upload->do_upload($namafield)) {            
            $gbr = $this->upload->data();                                    
            $gambar=$gbr['file_name'];
            $response = array('kode'=>200,'file_name'=>$gambar,'message' =>"upload berhasil");
        }else{
            $response = array('kode'=>201,'file_name'=>'default.png','message' =>$this->upload->display_errors());
        } 
         
        return $response;
    }      
}
