<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	public function __construct(){ 
        parent::__construct();        
        $this->load->model('M_user','user');
    }    

	public function index(){
		$this->load->view('admin/v_login');		
	}

	public function dashboard(){
                       
	    $data=array(
	            'body'=>'v_main',
	            'title'=>'Dashboard',
	            'navbar'=>'admin',
	            'index'=>'Dashboard',
    	        'controller'=>'main',
    	        'act'=>'dashboard',    	        
	        );
		  $this->load->view('template_default',$data);
	}

	public function login(){

        extract($_POST);

        $result=$this->user->check_login($user_name,$password);

        $data=$result;
		 
        if(!$result):
 
            echo '  <script>
                        alert("Login Gagal");
                        document.location="'.site_url('main').'";
                    </script>';

        else:
        	$session = array(
                           'user_name'  => $user_name,
                           'user_id'  => $data->user_id,
                           'id_role'  => $data->id_role,
                           'id_divisi'  => $data->id_divisi,
                           'id_jabatan'  => $data->id_jabatan,
                           'nik'  => $data->nik,
                           'nama_role'  => $data->nama_role,
                           'nama_karyawan'  => $data->nama_karyawan,
                           'logged_in' =>TRUE
                        );
               
                $this->session->set_userdata($session);
                
                redirect('main/dashboard');

        endif;
  }

  public function changePass(){
    $data = array(
      'title' => 'Ubah Kata Sandi',     
      'content' => $this->user->changePass(),
      'footer' => false,
    );
    $this->load->view("modal",$data);
  }

	public function logout(){
		$this->session->sess_destroy();        
        redirect('main');
	}	
}
