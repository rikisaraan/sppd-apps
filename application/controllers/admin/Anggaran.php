<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Anggaran extends CI_Controller {
	public function __construct(){
        parent::__construct();
        $this->load->model('M_Anggaran');        
    }

	public function index(){		
		$data=array(
	            'body'=>'v_anggaran',
	            'title'=>'Data Anggaran',
	            'navbar'=>'admin',
	            'index'=>'Data Anggaran',
    	        'controller'=>'Anggaran',
    	        'act'=>'daftar',
				"data"=>$this->M_Anggaran->getAnggaranAll()
	        );
		$this->load->view('template_default',$data);
	}

	public function add($id_anggaran=""){
		$type=($id_anggaran=="") ? "Tambah":"Perbarui";
		$data=array(
	            'body'=>'v_anggaran_add',
	            'title'=>$type.' Anggaran',
	            'navbar'=>'admin',
	            'index'=>'Data Anggaran',
    	        'controller'=>'Anggaran',
    	        'act'=>'tambah',
				"data"=>$this->M_Anggaran->add($id_anggaran)
	        );
		$this->load->view('template_default',$data);
	}

	public function delete($id_anggaran=''){
		$this->M_Anggaran->delete($id_anggaran);
	}

}
