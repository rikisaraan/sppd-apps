<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_role extends CI_Controller {
	public function __construct(){
        parent::__construct();
        $this->load->model('m_role');        
    }

	public function index(){		
		$data=array(
	            'body'=>'v_role',
	            'title'=>'Data role',
	            'navbar'=>'admin',
	            'index'=>'Data role',
    	        'controller'=>'c_role',
    	        'act'=>'daftar',
				"data"=>$this->m_role->getroleAll()
	        );
		$this->load->view('template_default',$data);
	}

	public function add($id_role=""){
		$type=($id_role=="") ? "Tambah":"Perbarui";
		$data=array(
	            'body'=>'v_role_add',
	            'title'=>$type.' role',
	            'navbar'=>'admin',
	            'index'=>'Data role',
    	        'controller'=>'c_role',
    	        'act'=>'tambah',
				"data"=>$this->m_role->add($id_role)
	        );
		$this->load->view('template_default',$data);
	}

	public function delete($id_role=''){
		$this->m_role->delete($id_role);
	}
	
	public function setRole($id_role=""){		
		$data=array(
	            'body'=>'v_role_set',
	            'title'=>'Set Role',
	            'navbar'=>'admin',
	            'index'=>'Data Role',
    	        'controller'=>'c_role',
    	        'act'=>'tambah',
    	        'id_role'=>$id_role,    	        
				"data"=>$this->m_role->setRole($id_role)
	        );
		$this->load->view('template_default',$data);
	}
}
