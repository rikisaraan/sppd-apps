<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Spt extends CI_Controller {
	public function __construct(){
        parent::__construct();
        $this->load->model('M_Spt');        
        $this->load->model('M_user');        
    }

	public function index(){		
		$data=array(
            'body'=>'v_spt',
            'title'=>'Data Surat Perintah Tugas',
            'navbar'=>'admin',
            'index'=>'Data Surat Perintah Tugas',
            'controller'=>'Spt',
            'act'=>'daftar',
            "data"=>$this->M_Spt->getSptAll()
        );
            
		$this->load->view('template_default',$data);
    }

    public function Report(){		
		$data=array(
            'body'=>'v_report_spt',
            'title'=>'Laporan SPT',
            'navbar'=>'admin',
            'index'=>'Laporan SPT',
            'controller'=>'Spt',
            'act'=>'daftar',
            "data"=>$this->M_Spt->getReportSpt(),
        );
            
		$this->load->view('template_default',$data);
	}
    
    public function antrianSpt(){		
		$data=array(
            'body'=>'v_list_spt_petugas',
            'title'=>'Data Surat Perintah Tugas Saya',
            'navbar'=>'admin',
            'index'=>'Data Surat Perintah Tugas Saya',
            'controller'=>'Spt',
            'act'=>'daftar',
            "data"=>$this->M_Spt->getSptByNik()
        );
            
		$this->load->view('template_default',$data);
	}

	public function add($id_memo=""){
		$type=($id_memo=="") ? "Tambah":"Perbarui";
		$data=array(
            'body'=>'v_spt_add',
            'title'=>$type.' Surat Perintah Tugas',
            'navbar'=>'admin',
            'index'=>'Data Surat Perintah Tugas',
            'controller'=>'Spt',
            'act'=>'tambah',
            "data"=>$this->M_Spt->add($id_memo)
        );
		$this->load->view('template_default',$data);
	}

	public function delete($id_memo=''){
		$this->M_Spt->delete($id_memo);
    }
    
    public function showDetail($no_spt="")
    {
        $data = array(
            'title' => 'Lihat Detail Surat Perintah Tugas',            
            'body' =>"admin/v_m_detail_spt",
            'data' =>$this->M_Spt->getSptRow($no_spt),
            'dataDetail'=>$this->M_Spt->getDetailSpt($no_spt),
            'footer' => false,
        );

        $this->load->view("modal",$data);
    }

    

}
