<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Lpd extends CI_Controller {
	public function __construct(){
        parent::__construct();
        $this->load->model('M_Lpd');        
    }

	public function index(){		
		$data=array(
            'body'=>'v_lpd',
            'title'=>'Data Laporan Perjalan Dinas',
            'navbar'=>'admin',
            'index'=>'Data Laporan Perjalan Dinas',
            'controller'=>'Lpd',
            'act'=>'daftar',
            "data"=>$this->M_Lpd->getLpdAll()
        );
            
		$this->load->view('template_default',$data);
	}

    public function Report(){		
		$data=array(
            'body'=>'v_report_lpd',
            'title'=>'Laporan Lpd',
            'navbar'=>'admin',
            'index'=>'Laporan Lpd',
            'controller'=>'Lpd',
            'act'=>'daftar',
            "data"=>$this->M_Lpd->getReportLpd(),
        );
            
		$this->load->view('template_default',$data);
    }
    
	public function add($id_lpd=""){
		$type=($id_lpd=="") ? "Tambah":"Perbarui";
		$data=array(
            'body'=>'v_lpd_add',
            'title'=>$type.' Laporan Perjalan Dinas',
            'navbar'=>'admin',
            'index'=>'Data Laporan Perjalan Dinas',
            'controller'=>'Lpd',
            'act'=>'tambah',
            "data"=>$this->M_Lpd->add($id_lpd)
        );
		$this->load->view('template_default',$data);
	}

	public function delete($id_lpd=''){
		$this->M_Lpd->delete($id_lpd);
    }
    
    public function showFile($id_lpd="")
    {
        $data = array(
            'title' => 'Lihat File Laporan Perjalan Dinas',            
            'body' =>"admin/v_m_lpd",
            'data' =>$this->M_Lpd->getLpdRow($id_lpd),
            'footer' => false,
        );

        $this->load->view("modal",$data);
    }

}
