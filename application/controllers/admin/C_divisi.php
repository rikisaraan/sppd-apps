<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class C_divisi extends CI_Controller {
	public function __construct(){
        parent::__construct();
        $this->load->model('m_divisi');        
    }

	public function index(){		
		$data=array(
	            'body'=>'v_divisi',
	            'title'=>'Data divisi',
	            'navbar'=>'admin',
	            'index'=>'Data divisi',
    	        'controller'=>'c_divisi',
    	        'act'=>'daftar',
				"data"=>$this->m_divisi->getdivisiAll()
	        );
		$this->load->view('template_default',$data);
	}

	public function add($id_divisi=""){
		$type=($id_divisi=="") ? "Tambah":"Perbarui";
		$data=array(
	            'body'=>'v_divisi_add',
	            'title'=>$type.' divisi',
	            'navbar'=>'admin',
	            'index'=>'Data divisi',
    	        'controller'=>'c_divisi',
    	        'act'=>'tambah',
				"data"=>$this->m_divisi->add($id_divisi)
	        );
		$this->load->view('template_default',$data);
	}

	public function delete($id_divisi=''){
		$this->m_divisi->delete($id_divisi);
	}

}
