<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_jabatan extends CI_Controller {
	public function __construct(){
        parent::__construct();
        $this->load->model('m_jabatan');        
    }

	public function index(){		
		$data=array(
	            'body'=>'v_jabatan',
	            'title'=>'Data jabatan',
	            'navbar'=>'admin',
	            'index'=>'Data jabatan',
    	        'controller'=>'c_jabatan',
    	        'act'=>'daftar',
				"data"=>$this->m_jabatan->getjabatanAll()
	        );
		$this->load->view('template_default',$data);
	}

	public function add($id_jabatan=""){
		$type=($id_jabatan=="") ? "Tambah":"Perbarui";
		$data=array(
	            'body'=>'v_jabatan_add',
	            'title'=>$type.' jabatan',
	            'navbar'=>'admin',
	            'index'=>'Data jabatan',
    	        'controller'=>'c_jabatan',
    	        'act'=>'tambah',
				"data"=>$this->m_jabatan->add($id_jabatan)
	        );
		$this->load->view('template_default',$data);
	}

	public function delete($id_jabatan=''){
		$this->m_jabatan->delete($id_jabatan);
	}
	
}
