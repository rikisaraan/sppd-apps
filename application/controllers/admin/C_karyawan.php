<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_karyawan extends CI_Controller {
	public function __construct(){
        parent::__construct();
        $this->load->model('m_karyawan');        
    }

	public function index(){		
		$data=array(
	            'body'=>'v_karyawan',
	            'title'=>'Data Karyawan',
	            'navbar'=>'admin',
	            'index'=>'Data Karyawan',
    	        'controller'=>'c_karyawan',
    	        'act'=>'daftar',
				"data"=>$this->m_karyawan->getKaryawanAll()
	        );
		$this->load->view('template_default',$data);
	}

	public function add($nik=""){
		$type=($nik=="") ? "Tambah":"Perbarui";
		$data=array(
	            'body'=>'v_karyawan_add',
	            'title'=>$type.' Karyawan',
	            'navbar'=>'admin',
	            'index'=>'Data Karyawan',
    	        'controller'=>'c_karyawan',
    	        'act'=>'tambah',
				"data"=>$this->m_karyawan->add($nik)
	        );
		$this->load->view('template_default',$data);
	}

	public function delete($nik=''){
		$this->m_karyawan->delete($nik);
	}
	
}
