<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Sppd extends CI_Controller {
	public function __construct(){
        parent::__construct();
        $this->load->model('M_Sppd');        
        $this->load->model('M_user');
        $this->createBy = $this->session->userdata("nik");            
    }

	public function index(){		
        $where=array("karyawan.nik"=>$this->createBy);
        $data=array(
            'body'=>'v_sppd',
            'title'=>'Data Surat Perintah Perjalanan Dinas',
            'navbar'=>'admin',
            'index'=>'Data Surat Perintah Perjalanan Dinas',
            'controller'=>'Sppd',
            'act'=>'daftar',
            "data"=>$this->M_Sppd->getSppdAll($where)
        );
            
		$this->load->view('template_default',$data);
    }
    
    public function Report(){		
		$data=array(
            'body'=>'v_report_sppd',
            'title'=>'Laporan Sppd',
            'navbar'=>'admin',
            'index'=>'Laporan Sppd',
            'controller'=>'Sppd',
            'act'=>'daftar',
            "data"=>$this->M_Sppd->getReportSppd(),
        );
            
		$this->load->view('template_default',$data);
	}
    
    public function Persetujuan(){		
        $id_divisi = $this->session->userdata("id_divisi");
        $where=array("divisi.id_divisi"=>$id_divisi,"sppd.sppd_status_id"=>"1");
        $data=array(
            'body'=>'v_sppd',
            'title'=>'Data Surat Perintah Perjalanan Dinas',
            'navbar'=>'admin',
            'index'=>'Data Surat Perintah Perjalanan Dinas',
            'controller'=>'Sppd',
            'act'=>'daftar',            
            "data"=>$this->M_Sppd->getSppdAll($where)
        );
            
		$this->load->view('template_default',$data);
	}

	public function add($no_Sppd=0,$id_sppd=0){
		$type=($id_sppd=="") ? "Buat":"Perbarui";
		$data=array(
            'body'=>'v_sppd_add',
            'title'=>$type.' Surat Perintah Perjalanan Dinas',
            'navbar'=>'admin',
            'index'=>'Data Surat Perintah Perjalanan Dinas',
            'controller'=>'Sppd',
            'act'=>'tambah',
            "data"=>$this->M_Sppd->add($no_Sppd,$id_sppd)
        );
		$this->load->view('template_default',$data);
	}

	public function delete($id_sppd=''){
		$this->M_Sppd->delete($id_sppd);
    }
    
    public function showDetail($id_sppd="")
    {
        $data = array(
            'title' => 'Lihat Detail Surat Perintah Perjalanan Dinas',            
            'body' =>"admin/v_m_detail_sppd",
            'data' =>$this->M_Sppd->getSppdRow($id_sppd),
            'footer' => false,
        );

        $this->load->view("modal",$data);
    }
    
    public function detailPersetujuanSppd($id_sppd="")
    {
        $data = array(
            'title' => 'Form Persetujuan Surat Perintah Perjalanan Dinas',            
            'body' =>"admin/v_m_persetujuan_sppd",
            'data' =>$this->M_Sppd->persetujuanSppd($id_sppd),
            'footer' => true,
        );

        $this->load->view("modal",$data);
    }



}
