<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_user extends CI_Controller {
	public function __construct(){
        parent::__construct();
		$this->load->model('m_user');
		$this->load->library('encryption');
    }

	public function index(){		
		
		$data=array(
	            'body'=>'v_user',
	            'title'=>'Data User',
	            'navbar'=>'admin',
	            'index'=>'Data Karyawan',
    	        'controller'=>'c_karyawan',
    	        'act'=>'tambah',
				"data"=>$this->m_user->getuserAll()
	        );
		$this->load->view('template_default',$data);
	}

	public function add($nik="",$id_user=""){
		$type=($id_user=="") ? "Tambah":"Perbarui";
		$data=array(
	            'body'=>'v_user_add',
	            'title'=>$type.' user',
	            'navbar'=>'admin',
	            'index'=>'Data Karyawan',
    	        'controller'=>'c_karyawan',
    	        'act'=>'detail',
    	        'detail'=>'Data User',
    	        'c_detail'=>'c_user',
				"data"=>$this->m_user->add($nik,$id_user)
	        );
		$this->load->view('template_default',$data);
	}

	public function delete($id_user=''){
		$this->m_user->delete($id_user);
	} 
	
}
