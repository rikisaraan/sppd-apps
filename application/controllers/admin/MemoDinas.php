<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MemoDinas extends CI_Controller {
	public function __construct(){
        parent::__construct();
        $this->load->model('M_MemoDinas');        
    }

	public function index(){		
		$data=array(
            'body'=>'v_memo_dinas',
            'title'=>'Data Memo Dinas',
            'navbar'=>'admin',
            'index'=>'Data Memo Dinas',
            'controller'=>'MemoDinas',
            'act'=>'daftar',
            "data"=>$this->M_MemoDinas->getMemoDinasAll()
        );
            
		$this->load->view('template_default',$data);
    }
    
    public function Report(){		
		$data=array(
            'body'=>'v_report_memo_dinas',
            'title'=>'Laporan Memo Dinas',
            'navbar'=>'admin',
            'index'=>'Laporan Memo Dinas',
            'controller'=>'MemoDinas',
            'act'=>'daftar',
            "data"=>$this->M_MemoDinas->getReportMemoDinas(),
        );
            
		$this->load->view('template_default',$data);
	}

	public function add($id_memo=""){
		$type=($id_memo=="") ? "Tambah":"Perbarui";
		$data=array(
            'body'=>'v_memo_dinas_add',
            'title'=>$type.' divisi',
            'navbar'=>'admin',
            'index'=>'Data Memo Dinas',
            'controller'=>'MemoDinas',
            'act'=>'tambah',
            "data"=>$this->M_MemoDinas->add($id_memo)
        );
		$this->load->view('template_default',$data);
	}

	public function delete($id_memo=''){
		$this->M_MemoDinas->delete($id_memo);
    }
    
    public function showFile($id_memo="")
    {
        $data = array(
            'title' => 'Lihat File Memo Dinas',            
            'body' =>"admin/v_m_memo_dinas",
            'data' =>$this->M_MemoDinas->getMemoDinasRow($id_memo),
            'footer' => false,
        );

        $this->load->view("modal",$data);
    }

    
}