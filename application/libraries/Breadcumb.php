<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Breadcumb {
        
        public function show($controller="",$title="",$index="",$act="",$detail="",$c_detail="",$v_detail="",$t_detail="",$v_detail_add="",$t_detail_add=""){
            /*<!-- Breadcrumb -->*/
            
            if($act=='dashboard'){
               $breadcumb='<li>
            		      		<a href='.site_url("Main/dashboard").'><i class="fa fa-home"></i> Home</a>                                    
            		    	</li>';
            }
            elseif($act=='daftar') {
                $breadcumb='<li>
            		      		<a href='.site_url("main/dashboard").'><i class="fa fa-home"></i> Home</a> <i class=""></i>
            		    	</li>
            				<li>
            					<a href='.site_url("admin/".$controller).'>'.$index.'</a>
            		    	</li>';
            }
            elseif($act=='detail'){
                $breadcumb='<li>
            		      		<a href='.site_url("main/dashboard").'><i class="fa fa-home"></i> Home</a> <i class=""></i>
            		    	</li>
            				<li>
            					<a href='.site_url("admin/".$controller).'>'.$index.'</a><i class=""></i>
            		    	</li>
            		    	<li>
            					<a href='.site_url("admin/".$c_detail).'>'.$detail.'</a><i class=""></i>
            		    	</li>
            				<li><a href="">'.$title.'</a></li>';
            }
            
            elseif($act=='detail_add'){
                $breadcumb='<li>
            		      		<a href='.site_url("main/dashboard").'><i class="fa fa-home"></i> Home</a> <i class=""></i>
            		    	</li>
            				<li>
            					<a href='.site_url("admin/".$controller).'>'.$index.'</a><i class=""></i>
            		    	</li>
            		    	<li>
            					<a href='.site_url("admin/".$c_detail).'>'.$detail.'</a><i class=""></i>
            		    	</li>
            		    	<li>
            					<a href='.site_url("admin/".$t_detail).'>'.$v_detail.'</a><i class=""></i>
            		    	</li>
            				<li><a href="">'.$title.'</a></li>';
            }
            
            elseif($act=='detail_add_detail'){
                $breadcumb='<li>
            		      		<a href='.site_url("main/dashboard").'><i class="fa fa-home"></i> Home</a> <i class=""></i>
            		    	</li>
            				<li>
            					<a href='.site_url("admin/".$controller).'>'.$index.'</a><i class=""></i>
            		    	</li>
            		    	<li>
            					<a href='.site_url("admin/".$c_detail).'>'.$detail.'</a><i class=""></i>
            		    	</li>
            		    	<li>
            					<a href='.site_url("admin/".$t_detail).'>'.$v_detail.'</a><i class=""></i>
            		    	</li>
            		    	<li>
            					<a href='.site_url("admin/".$t_detail_add).'>'.$v_detail_add.'</a><i class=""></i>
            		    	</li>
            		    	
            		    	
            				<li><a href="">'.$title.'</a></li>';
            }
            else{
                $breadcumb='<li>
            		      		<a href='.site_url("main/dashboard").'><i class="fa fa-home"></i> Home</a> <i class=""></i>
            		    	</li>
            				<li>
            					<a href='.site_url("admin/".$controller).'>'.$index.'</a><i class=""></i>
            		    	</li>
            				<li><a href="">'.$title.'</a></li>';
            }
            $breadcumb = '
            <h1>            
            <small>'.$title.'</small>
            </h1>
            <ol class="breadcrumb">
                '.$breadcumb.'
            </ol>';
            
            /*<!-- /Breadcrumb -->*/
            
            return $breadcumb;
        }
    }

?>