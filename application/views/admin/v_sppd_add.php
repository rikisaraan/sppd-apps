<form class="form-horizontal" action="<?php echo  current_url()?>" method="POST" enctype="multipart/form-data">
  <div class="box box-danger">
    <div class="box-header with-border">
      <h3 class="box-title">Form Surat Perintah Perjalanan Dinas</h3>
    </div>
    <div class="box-body">
      <div class="row">      
        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">        
          <div class="box-body">   
            <div class="form-group">
              <label for="tanggal" class="col-sm-4 control-label">Tanggal Berangkat</label>
              <div class="col-sm-8">
                <input type="text" readonly class="datepicker form-control" id="tanggal" placeholder="YYYY-MM-DD" value="<?php echo $formData->tanggal_berangkat?>" name="tanggal_berangkat" required >
              </div>
            </div>
            <div class="form-group">
              <label for="asal" class="col-sm-4 control-label">Asal</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="asal" placeholder="Contoh : Kota Binjai" value="<?php echo $formData->asal?>" name="asal" required>
              </div>
            </div>
            <div class="form-group">
              <label for="no_surat" class="col-sm-4 control-label">Nomor Surat</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="no_surat" placeholder="Contoh : SPPD/I/0001" value="<?php echo $formData->no_surat?>" name="no_surat" required>
              </div>
            </div>                                                        
            <div class="form-group">
              <label for="sppd_jarak" class="col-sm-4 control-label">Jarak</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="sppd_jarak" placeholder="Satuan Kilometer" value="<?php echo $formData->sppd_jarak?>" name="sppd_jarak" required>
              </div>
            </div>                                                        
          </div>          
        </div>
        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">        
          <div class="box-body">            
            <div class="form-group">
              <label for="tanggal" class="col-sm-4 control-label">Tanggal Pulang</label>
              <div class="col-sm-8">
                <input type="text" readonly class="datepicker form-control" id="tanggal" placeholder="YYYY-MM-DD" value="<?php echo $formData->tanggal_pulang?>" name="tanggal_pulang" required >
              </div>
            </div>
            <div class="form-group">
              <label for="tujuan" class="col-sm-4 control-label">Tujuan</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="tujuan" placeholder="Contoh : Kota Pekalongan" value="<?php echo $formData->tujuan?>" name="tujuan" required>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4 control-label">Transportasi</label>
            <div class="col-sm-8">
                <select class="form-control select2" name="transportasi">                  
                    <option  value="">Pilih Jenis Transportasi</option>
                    <option  <?php echo ($formData->transportasi=="kendaraan dinas") ? "selected":""; ?> value="kendaraan dinas">Kendaraan Dinas</option>
                    <option  <?php echo ($formData->transportasi=="kendaraan pribadi") ? "selected":""; ?> value="kendaraan pribadi">Kendaraan Pribadi</option>
                    <option  <?php echo ($formData->transportasi=="kendaraan umum") ? "selected":""; ?> value="kendaraan umum">Kendaraan Umum</option>
                </select>
            </div>
        </div>
        </div>
      </div>     
      <div class="box-footer">      
        <button type="submit" class="btn btn-info pull-right btn-flat bg-navy">Simpan</button>
      </div>
    </div>
    <!-- /.box-body -->
  </div>
</form>