<div class="box">
  <div class="box-header">
    <h3 class="box-title">Daftar Karyawan</h3>
    <a href="<?php echo site_url('admin/c_karyawan/add')?>" class="btn btn-flat bg-navy pull-right">Tambah &nbsp;<span class="fa fa-plus"></span></a>
  </div>
<!-- /.box-header -->
  <div class="box-body">
    <table id="datatable" class="table table-bordered table-striped">
      <thead>
        <tr>
          <th>NIK</th>
          <th>Nama</th>
          <th>Alamat</th>
		  <th>No. Telp</th>
          <th>Divisi</th>
          <th>Jabatan</th>
          <th>#</th>
        </tr>
      </thead>
      <tbody>
        <?php
          
          foreach($data->result() as $k) {
            
            $qUser = $this->db->get_where("user",array("nik"=>$k->nik));
            $hide=($qUser->num_rows()<1) ? "":"style='display:none'";
            
            echo"<tr>
                    <td>".$k->nik."</td>
                    <td>".$k->nama_karyawan."</td>
                    <td>".$k->alamat."</td>
					<td>".$k->no_telp."</td>
                    <td>".$k->nama_divisi."</td>
                    <td>".$k->nama_jabatan."</td>
                    <td align='center'>
                      <a data-toggle='tooltip' data-placement='top' title='Perbarui' href='".site_url('admin/c_karyawan/add/'.$k->nik)."' class='btn btn-flat bg-maroon'><span class='fa fa-edit'></span></a>
                      <a onclick=\"return confirm('Yakin ingin menghapus data ini')\" data-toggle='tooltip' data-placement='top' title='Hapus' href='".site_url('admin/c_karyawan/delete/'.$k->nik)."' class='btn btn-flat bg-maroon'><span class='fa fa-trash'></span></a>
                      <a ".$hide." data-toggle='tooltip' data-placement='top' title='Set User' href='".site_url('admin/c_user/add/'.$k->nik)."' class='btn btn-flat bg-maroon'><span class='fa fa-arrow-circle-right'></span></a>
                    </td>
                </tr>";          
          }

        ?>
      </tbody>      
    </table>
  </div>
<!-- /.box-body -->
</div>
<!-- /.box -->