<form class="form-horizontal" action="<?php echo  current_url()?>" method="POST" enctype="multipart/form-data">
  <div class="box box-danger">
    <div class="box-header with-border">
      <h3 class="box-title">Form Laporan Perjalanan Dinas</h3>
    </div>
    <div class="box-body">
      <div class="row">      
        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">        
          <div class="box-body">                        
            <div class="form-group">
              <label for="tanggal_kegiatan" class="col-sm-4 control-label">Tanggal Kegiatan</label>
              <div class="col-sm-8">
                <input type="text" readonly class="datepicker form-control" id="tanggal_kegiatan" placeholder="" value="<?php echo $formData->tanggal_kegiatan?>" name="tanggal_kegiatan" required>
              </div>
            </div>
            <div class="form-group">
              <label for="kegiatan" class="col-sm-4 control-label">Kegiatan</label>
              <div class="col-sm-8">
                <textarea name="kegiatan" class="form-control" placeholder="Contoh : Memperbaiki sistem POS" rows="5" required><?php echo $formData->kegiatan?></textarea>
              </div>
            </div>
            <div class="form-group">
              <label for="laporan" class="col-sm-4 control-label">Laporan</label>
              <div class="col-sm-8">
                <textarea name="laporan" class="form-control" placeholder="Contoh : Sistem POS sudah diperbaiki sesuai dengan perintah" rows="5" required><?php echo $formData->laporan?></textarea>
              </div>
            </div>          
          </div>          
        </div>
        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">        
          <div class="box-body">            
            <div class="form-group">
              <label for="tempat" class="col-sm-4 control-label">Tempat</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="tempat" placeholder="" value="<?php echo $formData->tempat?>" name="tempat">
              </div>
            </div>
            <div class="form-group">
              <label for="keterangan" class="col-sm-4 control-label">Keterangan</label>
              <div class="col-sm-8">
                <textarea name="keterangan" class="form-control" placeholder="Contoh : Ada beberapa catatan yang harus di jaga antara lain ..." rows="5" required><?php echo $formData->keterangan?></textarea>
              </div>
            </div>
            <div class="form-group"> 
              	<label for="klaim" class="col-sm-4 control-label">File Klaim Kwitansi</label>
              	<div class="col-sm-8">
                	<input type="file" class="form-control" name="klaim" required> <b class="text-red">* Support gambar gif | jpg | png | jpeg</b>
              	</div>
            </div> 
          </div>
        </div>
      </div>
      <div class="box-footer">      
        <button type="submit" class="btn btn-info pull-right btn-flat bg-navy">Simpan</button>
      </div>
    </div>
    <!-- /.box-body -->
  </div>
</form> 