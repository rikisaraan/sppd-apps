<div class="box">
  <div class="box-header">
    <h3 class="box-title">Daftar Surat Perintah Perjalanan Dinas</h3>
  </div>
<!-- /.box-header -->
  <div class="box-body">
    <table id="datatable" class="table table-bordered table-striped">
      <thead>
        <tr>
          <th>No Surat</th>
          <th>Tanggal Berangkat</th>
          <th>Tanggal Pulang</th>
          <th>Asal</th>
          <th>Tujuan</th>
          <th>Transportasi</th>
          <th>Status</th>
          <th>#</th>
        </tr>
      </thead>
      <tbody>
        <?php   
			//looping data untuk nampilkan data by db
          foreach($data->result() as $k) {
            $method = ($this->router->fetch_method()=="Persetujuan") ? "detailPersetujuanSppd" : "showDetail";
            $title = ($this->router->fetch_method()=="Persetujuan") ? "Persetujuan SPPD" : "Lihat Detail";
                          
            echo"<tr>
                    <td>".$k->no_surat."</td>
                    <td>".$k->tanggal_berangkat."</td>
                    <td>".$k->tanggal_pulang."</td>
                    <td>".$k->asal."</td>
                    <td>".$k->tujuan."</td>
                    <td>".$k->transportasi."</td>
                    <td><label class='label ".$k->sppd_status_color."'>".ucwords($k->sppd_status_name)."</label></td>
                    <td align='center'>
                      <a data-toggle='tooltip' data-placement='top' title='Perbarui' href='".site_url('admin/Sppd/add/'.$k->no_spt."/".$k->id_sppd)."' class='btn btn-flat bg-maroon'><span class='fa fa-edit'></span></a>                      
                      <a onclick=\"return confirm('Yakin ingin menghapus data ini?')\" data-toggle='tooltip' data-placement='top' title='Hapus' href='".site_url('admin/Sppd/delete/'.$k->id_sppd)."' class='btn btn-flat bg-maroon'><span class='fa fa-trash'></span></a>
                      <a title='".$title."' href='".site_url('admin/Sppd/'.$method.'/'.$k->id_sppd)."' class='btn btn-flat bg-maroon' data-toggle='modal' data-target='#myModal'><span class='fa fa-eye'></span></a>
                    </td>
                </tr>";          
          }

        ?>
      </tbody>      
    </table>
  </div>
<!-- /.box-body -->
</div>
<!-- /.box -->