<?php
  $nik = ($formData->nik=="") ? $autoNik: $formData->nik;  
  $selectedL = ($formData->jenis_kelamin=="Laki-laki") ? "checked":"" ;
  $selectedP = ($formData->jenis_kelamin=="Perempuan") ? "checked":"" ;
  $selectedB = ($formData->status_karyawan=="Belum Menikah") ? "checked":"" ;
  $selectedS = ($formData->status_karyawan=="Sudah Menikah") ? "checked":"" ;
?>
<form class="form-horizontal" action="<?php echo  current_url()?>" method="POST">
  <div class="box box-danger">
    <div class="box-header with-border">
      <h3 class="box-title">Form Karyawan</h3>
    </div> 
    <div class="box-body">
      <div class="row">      
        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">        
          <div class="box-body">
            <div class="form-group">
              <label for="nik" class="col-sm-4 control-label">NIK</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="nik" placeholder="" name="nik" value="<?php echo $nik?>" readonly>
              </div>
            </div>
            <div class="form-group">
              <label for="nama_karyawan" class="col-sm-4 control-label">Nama Karyawan</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="nama_karyawan" placeholder="" value="<?php echo $formData->nama_karyawan?>" name="nama_karyawan" required>
              </div>
            </div>
            <div class="form-group">
              <label for="tempat_lahir" class="col-sm-4 control-label">Tempat Lahir</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="tempat_lahir" placeholder="" name="tempat_lahir" value="<?php echo $formData->tempat_lahir?>" required>
              </div>
            </div>
            <div class="form-group">
              <label for="tgl_lahir" class="col-sm-4 control-label">Tanggal Lahir</label>
              <div class="col-sm-8">                
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>                  
                  <input type="text" class="form-control pull-right datepicker" id="tgl_lahir" name="tgl_lahir" readonly value="<?php echo $formData->tgl_lahir?>" required>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="no_telp" class="col-sm-4 control-label">No Telepon</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="no_telp" placeholder="" name="no_telp" value="<?php echo $formData->no_telp?>" data-inputmask='"mask": "(999) 999-999-999"' data-mask required>
              </div>
            </div>
            <div class="form-group">
              <label for="no_telp_darurat" class="col-sm-4 control-label">No Telepon Darurat</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="no_telp_darurat" placeholder="" name="no_telp_darurat" value="<?php echo $formData->no_telp_darurat?>" data-inputmask='"mask": "(999) 999-999-999"' data-mask>
              </div>
            </div>
            <div class="form-group">
              <label for="email" class="col-sm-4 control-label">Email</label>
              <div class="col-sm-8">
                <input type="email" class="form-control" id="email" placeholder="" value="<?php echo $formData->email?>" name="email" required>
              </div>
            </div>
          </div>          
        </div>
        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">        
          <div class="box-body">            
            <div class="form-group">
              <label for="tgl_masuk_kerja" class="col-sm-4 control-label">Tanggal Masuk Kerja</label>
              <div class="col-sm-8">                
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>                  
                  <input type="text" class="form-control pull-right datepicker" id="tgl_masuk_kerja" value="<?php echo $formData->tgl_masuk_kerja?>" name="tgl_masuk_kerja" readonly required>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="alamat" class="col-sm-4 control-label">Alamat</label>
              <div class="col-sm-8">
                <textarea name="alamat" class="form-control" rows="3" placeholder="" required><?php echo $formData->alamat?></textarea>
              </div>
            </div>
            <div class="form-group">
              <label for="jenis_kelamin" class="col-sm-4 control-label">Jenis Kelamin</label>
              <div class="col-sm-8">
                <label>
                  <input type="radio" name="jenis_kelamin" class="flat-red" value="Laki-laki" <?php echo $selectedL?>>&nbsp;Laki-Laki
                </label>
                <label>
                  <input type="radio" name="jenis_kelamin" class="flat-red" value="Perempuan" <?php echo $selectedP?>>&nbsp;Perempuan
                </label>              
              </div>
            </div>
            <div class="form-group">
              <label for="status_karyawan" class="col-sm-4 control-label">Status Karyawan</label>
              <div class="col-sm-8">
                <label>
                  <input type="radio" name="status_karyawan" class="flat-red" value="Belum Menikah" <?php echo $selectedB?>>&nbsp;Belum Menikah
                </label>
                <label>
                  <input type="radio" name="status_karyawan" class="flat-red" value="Sudah Menikah" <?php echo $selectedS?>>&nbsp;Sudah Menikah
                </label>              
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-4 control-label">Agama</label>
              <div class="col-sm-8">
                <select class="form-control select2" name="agama" required>
                  <option value="">Pilih Agama</option>
                  <option value="Islam" <?php echo ($formData->agama=="Islam") ? "selected":"";?>>Islam</option>
                  <option value="Katolik" <?php echo ($formData->agama=="Katolik") ? "selected":"";?>>Katolik</option>
                  <option value="Protestan" <?php echo ($formData->agama=="Protestan") ? "selected":"";?>>Protestan</option>
                  <option value="Budha" <?php echo ($formData->agama=="Budha") ? "selected":"";?>>Budha</option>
                  <option value="Hindu" <?php echo ($formData->agama=="Hindu") ? "selected":"";?>>Hindu</option>
                  <option value="Kongucu" <?php echo ($formData->agama=="Kongucu") ? "selected":"";?>>Konghucu</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-4 control-label">Divisi</label>
              <div class="col-sm-8">
                <select class="form-control select2" name="id_divisi" required>
                <option value="">Pilih Divisi</option>
                <?php
                  foreach ($dataDivisi->result() as $divisi) {
                    $selected = ($divisi->id_divisi==$formData->id_divisi) ? "selected":"";
                    echo"<option value='".$divisi->id_divisi."' ".$selected.">".$divisi->nama_divisi."</option>";
                  }
                ?>                  
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-4 control-label">Jabatan</label>
              <div class="col-sm-8">
                <select class="form-control select2" name="id_jabatan" required>
                <option value="">Pilih Jabatan</option>
                <?php
                  foreach ($dataJabatan->result() as $jabatan) {
                    $selected = ($jabatan->id_jabatan==$formData->id_jabatan) ? "selected":"";
                    echo"<option value='".$jabatan->id_jabatan."' ".$selected.">".$jabatan->nama_jabatan."</option>";
                  }
                ?>                  
                </select>
              </div>
            </div>
          </div>                  
        </div>            
      </div>
      <div class="box-footer">      
        <button type="submit" class="btn btn-info pull-right btn-flat bg-navy">Simpan</button>
      </div>
    </div>
    <!-- /.box-body -->
  </div>
</form> 