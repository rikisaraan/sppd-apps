<div class="box box-danger">
    <div class="box-header with-border">
      <h3 class="box-title">Data Profile Staf</h3>
    </div>    
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div class="box-body">
                <table class="table table-bordered">    
                    <tbody>
                        <tr>
                            <td width="15%">Nik </td>
                            <td width="35%"><?php echo $data->nik?></td>
                            <td width="15%">Nama Karyawan</td>
                            <td width="35%"><?php echo $data->nama_karyawan?></td>                            
                        </tr>
                        <tr>
                            <td width="15%">Jabatan </td>
                            <td width="35%"><?php echo $data->nama_jabatan?></td>
                            <td width="15%">Nama Divisi</td>
                            <td width="35%"><?php echo $data->nama_divisi?></td>                            
                        </tr>
                        <tr>
                            <td width="15%">Uang Makan </td>
                            <td width="35%"><?php echo rupiah($data->uang_makan)?></td>
                            <td width="15%">Uang Saku</td>
                            <td width="35%"><?php echo rupiah($data->uang_saku)?></td>                            
                        </tr>
                        <tr>
                            <td width="15%">Uang Penginapan </td>
                            <td width="35%"><?php echo rupiah($data->uang_penginapan)?></td>
                        </tr>
                    </tbody>
                </table>    
            </div>            
        </div>
    </div>
</div>

<div class="box box-danger">
    <div class="box-header with-border">
      <h3 class="box-title">Detail Surat Perintah Perjalanan Dinas</h3>
    </div>    
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div class="box-body">
                <table class="table table-bordered">    
                    <tbody>
                        <tr>
                            <td width="15%">Tanggal Berangkat </td>
                            <td width="35%"><?php echo $data->tanggal_berangkat?></td>
                            <td width="15%">Tanggal Pulang</td>
                            <td width="35%"><?php echo $data->tanggal_pulang?></td>                            
                        </tr>
                        <tr>
                            <td width="15%">Asal </td>
                            <td width="35%"><?php echo $data->asal?></td>
                            <td width="15%">Tujuan </td>
                            <td width="35%"><?php echo $data->tujuan?></td>
                        </tr>
                        <tr>
                            <td width="15%">Lama Perjalanan </td>
                            <td width="35%"><?php echo $data->lama_perjalanan?> Hari</td>
                            <td width="15%">Transportasi </td>
                            <td width="35%"><?php echo $data->transportasi?></td>                            
                        </tr>
                        <tr>   
                            <td width="15%">No Surat </td>
                            <td width="35%"><?php echo $data->no_surat?></td>                            
                            <td width="15%">Status</td>
                            <td width="35%"><?php echo $data->sppd_status_name?></td>
                        </tr>
                    </tbody>
                </table>    
            </div>            
        </div>
    </div>
</div>

<div class="box box-danger">
    <div class="box-header with-border">
      <h3 class="box-title">Form Persetujuan Perintah Perjalanan Dinas</h3>
    </div>    
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div class="box-body">
                <table class="table table-bordered">    
                    <tbody>
                        <tr>
                            <td width="50%" >
                                <label class="pull-right">
                                    <input type="hidden" class="form-control" value="<?php echo $data->id_sppd?>" name="id_sppd">
                                    <input type="radio" name="sppd_status_id" class="flat-red" value="2">&nbsp;Setujui Pengajuan
                                </label>
                            </td>
                            <td width="50%" >
                                <label class="pull-left">
                                    <input type="radio" name="sppd_status_id" class="flat-red" value="3">&nbsp;Tolak Pengajuan
                                </label>
                            </td>
                        </tr>
                        
                    </tbody>
                </table>    
            </div>            
        </div>
    </div>
</div>

