<div class="box">
  <div class="box-header">
    <h3 class="box-title">Daftar Jabatan</h3>
    <a href="<?php echo site_url('admin/c_jabatan/add')?>" class="btn btn-flat bg-navy pull-right">Tambah &nbsp;<span class="fa fa-plus"></span></a>
  </div>
<!-- /.box-header -->
  <div class="box-body">
    <table id="datatable" class="table table-bordered table-striped">
      <thead>
        <tr>
          <th>Id Jabatan</th>
          <th>Nama</th>          
          <th>Keterangan</th>
          <th>#</th>
        </tr>
      </thead>
      <tbody>
        <?php          
          foreach($data->result() as $k) {
            echo"<tr>
                    <td>".$k->id_jabatan."</td>
                    <td>".$k->nama_jabatan."</td>
                    <td>".$k->keterangan."</td>
                    <td align='center'>
                      <a data-toggle='tooltip' data-placement='top' title='Perbarui' href='".site_url('admin/c_jabatan/add/'.$k->id_jabatan)."' class='btn btn-flat bg-maroon'><span class='fa fa-edit'></span></a>
                      <a onclick=\"return confirm('Yakin ingin menghapus data ini?')\" data-toggle='tooltip' data-placement='top' title='Hapus' href='".site_url('admin/c_jabatan/delete/'.$k->id_jabatan)."' class='btn btn-flat bg-maroon'><span class='fa fa-trash'></span>
                    </td>
                </tr>";          
          }

        ?>
      </tbody>      
    </table>
  </div>
<!-- /.box-body -->
</div>
<!-- /.box -->