<form class="form-horizontal" action="<?php echo  current_url()?>" method="POST">
  <div class="box box-danger">
    <div class="box-header with-border">
      <h3 class="box-title">Form jabatan</h3>
    </div>
    <div class="box-body">
      <div class="row">      
        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">        
          <div class="box-body">            
            <div class="form-group">
              <label for="nama_jabatan" class="col-sm-4 control-label">Nama jabatan</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="nama_jabatan" placeholder="" value="<?php echo $formData->nama_jabatan?>" name="nama_jabatan" required>
              </div>
            </div>            
          </div>          
        </div>
        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">        
          <div class="box-body">            
            <div class="form-group">
              <label for="keterangan" class="col-sm-4 control-label">Keterangan</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="keterangan" placeholder="" value="<?php echo $formData->keterangan?>" name="keterangan">
              </div>
            </div>
          </div>                  
        </div>            
      </div>
      <div class="box-footer">      
        <button type="submit" class="btn btn-info pull-right btn-flat bg-navy">Simpan</button>
      </div>
    </div>
    <!-- /.box-body -->
  </div>
</form> 