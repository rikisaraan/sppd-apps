
<div class="box box-danger">
    <div class="box-header with-border">
      <h3 class="box-title">Detail Surat Perintah Tugas</h3>
    </div>    
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div class="box-body">
                <table class="table table-bordered">    
                    <tbody>
                        <tr>
                            <td width="15%">Memo Dinas </td>
                            <td width="35%"><?php echo $data->nomor_memo." - ".$data->pengirim?></td>
                            <td width="15%">Tempat Tugas </td>
                            <td width="35%"><?php echo $data->tempat?></td>
                        </tr>
                        <tr>
                            <td width="15%">Tanggal </td>
                            <td width="35%"><?php echo $data->tanggal?></td>
                            <td width="15%">Lama Tugas </td>
                            <td width="35%"><?php echo $data->lama?> Hari</td>
                        </tr>
                        <tr>
                            <td width="15%">Kegiatan </td>
                            <td width="35%" colspan="3"><?php echo $data->kegiatan?></td>
                        </tr>
                    </tbody>
                </table>    
            </div>            
        </div>
    </div>
  </div>
  
<div class="box box-danger">
    <div class="box-header with-border">
        <h3 class="box-title">Data Petugas</h3>
    </div>    
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div class="box-body">
                <table class="table table-bordered">
                    <thead>                                                                  
                        <tr>
                            <th width="15%">NIK</th>                          
                            <th>Nama</th>                          
                            <th width="20%">Divisi</th>                          
                            <th width="20%">Jabatan</th>                          
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            foreach($dataDetail as $staf){
                                echo "  <tr>
                                            <td>".$staf->nik."</td>
                                            <td>".$staf->nama_karyawan."</td>
                                            <td>".$staf->nama_divisi."</td>
                                            <td>".$staf->nama_jabatan."</td>
                                        </tr>";                                
                            }
                        ?>
                    </tbody>
                </table>  
            </div>            
        </div>
    </div>
</div>