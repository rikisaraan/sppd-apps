<div class="box">
  <div class="box-header">
    <h3 class="box-title">Daftar Anggaran</h3>
    <a href="<?php echo site_url('admin/Anggaran/add')?>" class="btn btn-flat bg-navy pull-right">Tambah &nbsp;<span class="fa fa-plus"></span></a>
  </div>
<!-- /.box-header -->
  <div class="box-body">
    <table id="datatable" class="table table-bordered table-striped">
      <thead>
        <tr>
          <th>Jabatan</th>          
          <th>Uang Makan</th>          
          <th>Uang Saku</th>          
          <th>Uang Penginapan</th>                    
          <th>Uang Transport</th>                    
          <th>#</th>
        </tr>
      </thead>
      <tbody>
        <?php   
			//looping data untuk nampilkan data by db
          foreach($data->result() as $k) {
            echo"<tr>
                    <td>".$k->nama_jabatan."</td>          
                    <td>".rupiah($k->uang_makan)."</td>
                    <td>".rupiah($k->uang_saku)."</td>
                    <td>".rupiah($k->uang_penginapan)."</td>                                      
                    <td>".rupiah($k->uang_transport)."</td>                                      
                    <td align='center'>
                      <a data-toggle='tooltip' data-placement='top' title='Perbarui' href='".site_url('admin/Anggaran/add/'.$k->id_anggaran)."' class='btn btn-flat bg-maroon'><span class='fa fa-edit'></span></a>
                      <a onclick=\"return confirm('Yakin ingin menghapus data ini?')\" data-toggle='tooltip' data-placement='top' title='Hapus' href='".site_url('admin/Anggaran/delete/'.$k->id_anggaran)."' class='btn btn-flat bg-maroon'><span class='fa fa-trash'></span>
                    </td>
                </tr>";          
          }

        ?>
      </tbody>      
    </table>
  </div>
<!-- /.box-body -->
</div>
<!-- /.box -->