<form class="form-horizontal" action="<?php echo  current_url()?>" method="POST">
  <div class="box box-danger">
    <div class="box-header with-border">
      <h3 class="box-title">Form Filter Laporan SPT</h3>
    </div>
    <div class="box-body">      
      <div class="row" style="margin-top:30px;">
        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 offset-md-3">
          <div class="box-body">            
            <div class="form-group">
              <label for="nama_proyek" class="col-sm-3 control-label">Periode Awal</label>
              <div class="col-sm-9">
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>                  
                  <input type="text" class="form-control datepicker" placeholder="" value="" name="periode_awal" readonly>
                </div>
              </div>              
            </div>
            <div class="form-group">
              <label class="col-sm-3 control-label">Divisi</label>
              <div class="col-sm-9">                
                <select class="form-control select2" name="id_divisi">                  
                  <option  value="">Pilih Divisi</option>
                <?php
                  
                  foreach ($dataDivisi->result() as $divisi) {
                    echo"<option ".$selected." value='".$divisi->id_divisi."' >".$divisi->nama_divisi."</option>";
                  }
                ?>                  
                </select>
              </div>
            </div>
          </div>
        </div>
        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 offset-md-3">
          <div class="box-body">            
            <div class="form-group">
              <label for="periode_akhir" class="col-sm-3 control-label">Periode Akhir</label>
              <div class="col-sm-9">
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>                  
                  <input type="text" class="form-control datepicker" placeholder="" value="" name="periode_akhir" readonly>
                </div>
              </div>
            </div>            
            <div class="form-group">
              <label class="col-sm-3 control-label">Karyawan</label>
              <div class="col-sm-9">                
                <select class="form-control select2" name="nik">                  
                  <option  value="">Pilih Karyawan</option>
                <?php                  
                  foreach ($dataKaryawan->result() as $karyawan) {                    
                    echo"<option value='".$karyawan->nik."' >".$karyawan->nik." - ".$karyawan->nama_karyawan."</option>";
                  }
                ?>                  
                </select>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="box-footer">      
        <button type="submit" class="btn btn-info pull-right btn-flat bg-navy">Cari</button>
      </div>
    </div>
    <!-- /.box-body -->
  </div>
<?php
  if($_POST){    
    $row="";
    if($querySummary->num_rows()==0){
    	$row="<tr><td colspan='9' align='center'>Data Tidak Ada</td></tr>";
    }else{
    	foreach ($querySummary->result() as $d) {
	   		
	      	$row.="<tr>
	              	<td>".$d->nik."</td>
	              	<td>".$d->nama_karyawan."</td>
	              	<td>".$d->nama_divisi."</td>
	              	<td>".$d->tanggal_kegiatan."</td>
	              	<td>".$d->tempat." Hari</td>
	              	<td>".$d->kegiatan."</td>
	              	<td>".$d->laporan."</td>
	              	<td>".$d->keterangan."</td>
	            </tr>";
	    }
    }    
?>
    <div class="box box-danger">
      <div class="box-header with-border">
        <h3 class="box-title">Hasil Pencarian</h3>
      </div>
      <div class="box-body">
        <div class="row">      
          <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12  ">
            <div class="box-body">
                <table id="datatableWithButton" class="table table-bordered table-striped">
                 <thead>
                    <tr>
                      <th>Nik</th>
                      <th>Nama</th>
                      <th>Divisi</th>
                      <th>Tanggal Kegiatan</th>
                      <th>Tempat</th>
                      <th>Kegiatan</th>
                      <th>Laporan</th>
                      <th>Keterangan</th>
                    </tr>
                    </thead>
                    <tbody><?php echo $row?></tbody>
                </table>
            </div>
          </div>       
        </div>
        <div class="box-footer" style="display: none;">
          <a href="<?php echo site_url('admin/c_report/cetakLaporanCutiIzinExcel/'.$periode_awal.'/'.$periode_akhir.'/'.$id_divisi.'/'.$nik.'/'.$status.'/'.$type)?>" class="btn btn-success btn-flat pull-right">Download Excel &nbsp;<span class="fa fa-file-excel-o"></span></a>
          <!-- <a href="" class="btn btn-danger btn-flat pull-right">Download PDF &nbsp;<span class="fa fa-file-pdf-o"></span></a> -->
        </div>
      </div>
      <!-- /.box-body -->
    </div>
<?php
  }
?>
</form> 
