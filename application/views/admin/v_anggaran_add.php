<form id="form" class="form-horizontal" action="<?php echo  current_url()?>" method="POST">
  <div class="box box-danger">
    <div class="box-header with-border">
      <h3 class="box-title">Form Anggaran</h3>
    </div>
    <div class="box-body">
      <div class="row">      
        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">        
          <div class="box-body">
            <div class="form-group">
                <label class="col-sm-4 control-label">Jabatan</label>
                <div class="col-sm-8">
                    <select class="form-control select2" name="id_jabatan">                  
                        <option  value="">Pilih Jabatan</option>
                        <?php                  
                            foreach ($dataJabatan->result() as $jabatan) {                    
                                $selected=($jabatan->id_jabatan==$formData->id_jabatan) ? "selected" :"" ;
                                echo"<option ".$selected." value='".$jabatan->id_jabatan."' >".$jabatan->nama_jabatan."</option>";
                            }
                        ?>                  
                    </select>
                </div>
            </div>       
            <div class="form-group">
              <label for="uang_makan" class="col-sm-4 control-label">Uang Makan</label>
              <div class="col-sm-8">
                <input type="text" class="form-control rupiah" value="<?php echo $formData->uang_makan?>" name="uang_makan" required>
              </div>
            </div> 
            <div class="form-group">
              <label for="uang_makan" class="col-sm-4 control-label">Uang Transport</label>
              <div class="col-sm-8">
                <input type="text" class="form-control rupiah" value="<?php echo $formData->uang_transport?>" name="uang_transport" required>
              </div>
            </div>           
          </div>          
        </div>
        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">        
          <div class="box-body">            
            <div class="form-group">
              <label for="uang_saku" class="col-sm-4 control-label">Uang Saku</label>
              <div class="col-sm-8">
                <input type="text" class="form-control rupiah" value="<?php echo $formData->uang_saku?>" name="uang_saku">
              </div>
            </div>
            <div class="form-group">
              <label for="uang_penginapan" class="col-sm-4 control-label">Uang Penginapan</label>
              <div class="col-sm-8">
                <input type="text" class="form-control rupiah" value="<?php echo $formData->uang_penginapan?>" name="uang_penginapan">
              </div>
            </div>
          </div>                  
        </div>            
      </div>
      <div class="box-footer">      
        <button type="submit" class="btn btn-info pull-right btn-flat bg-navy">Simpan</button>
      </div>
    </div>
    <!-- /.box-body -->
  </div>
</form> 

<script>
  $(function(){
    $(".rupiah").keyup(function(e){
      this.value = this.value.replace(/[^0-9]/g, '');
      $(this).val(formatRupiah($(this).val()));
    });
  });
</script>