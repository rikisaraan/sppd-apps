<div class="box">
  <div class="box-header">
    <h3 class="box-title">Daftar Surat Perintah Tugas</h3>
    <a href="<?php echo site_url('admin/Spt/add')?>" class="btn btn-flat bg-navy pull-right">Tambah &nbsp;<span class="fa fa-plus"></span></a>
  </div>
<!-- /.box-header -->
  <div class="box-body">
    <table id="datatable" class="table table-bordered table-striped">
      <thead>
        <tr>
          <th>Tanggal</th>
          <th>Nomor Memo</th>
          <th>Lama</th>
          <th>Tempat</th>
          <th>Kegiatan</th>
          <th>#</th>
        </tr>
      </thead>
      <tbody>
        <?php   
			//looping data untuk nampilkan data by db
          foreach($data->result() as $k) {
            echo"<tr>
                    <td>".$k->tanggal."</td>
                    <td>".$k->nomor_memo."</td>
                    <td>".$k->lama."</td>
                    <td>".$k->tempat."</td>
                    <td>".$k->kegiatan."</td>
                    <td align='center'>
                      <a data-toggle='tooltip' data-placement='top' title='Perbarui' href='".site_url('admin/Spt/add/'.$k->no_spt)."' class='btn btn-flat bg-maroon'><span class='fa fa-edit'></span></a>                      
                      <a onclick=\"return confirm('Yakin ingin menghapus data ini?')\" data-toggle='tooltip' data-placement='top' title='Hapus' href='".site_url('admin/Spt/delete/'.$k->no_spt)."' class='btn btn-flat bg-maroon'><span class='fa fa-trash'></span></a>
                      <a title='Lihat Detail' href='".site_url('admin/Spt/showDetail/'.$k->no_spt)."' class='btn btn-flat bg-maroon' data-toggle='modal' data-target='#myModal'><span class='fa fa-eye'></span></a>
                    </td>
                </tr>";          
          }

        ?>
      </tbody>      
    </table>
  </div>
<!-- /.box-body -->
</div>
<!-- /.box -->