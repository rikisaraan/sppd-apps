<form class="form-horizontal" action="<?php echo  current_url()?>" method="POST" enctype="multipart/form-data">
  <div class="box box-danger">
    <div class="box-header with-border">
      <h3 class="box-title">Form Memo Dinas</h3>
    </div>
    <div class="box-body">
      <div class="row">      
        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">        
          <div class="box-body">            
            <div class="form-group">
              <label for="nomor_memo" class="col-sm-4 control-label">Nomor Memo</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="nomor_memo" placeholder="" value="<?php echo $formData->nomor_memo?>" name="nomor_memo" required>
              </div>
            </div>
            <div class="form-group">
              <label for="tanggal" class="col-sm-4 control-label">Tanggal</label>
              <div class="col-sm-8">
                <input type="text" readonly class="datepicker form-control" id="tanggal" placeholder="" value="<?php echo $formData->tanggal?>" name="tanggal" required>
              </div>
            </div>
          </div>          
        </div>
        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">        
          <div class="box-body">            
            <div class="form-group">
              <label for="pengirim" class="col-sm-4 control-label">Pengirim</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="pengirim" placeholder="" value="<?php echo $formData->pengirim?>" name="pengirim">
              </div>
            </div>
            <div class="form-group"> 
              	<label for="lampiran" class="col-sm-4 control-label">File Memo Dinas</label>
              	<div class="col-sm-8">
                	<input type="file" class="form-control" name="lampiran">	<b class="text-red">* Support gambar gif | jpg | png | jpeg</b>
              	</div>
            </div>
          </div>
        </div>
      </div>
      <div class="box-footer">      
        <button type="submit" class="btn btn-info pull-right btn-flat bg-navy">Simpan</button>
      </div>
    </div>
    <!-- /.box-body -->
  </div>
</form> 