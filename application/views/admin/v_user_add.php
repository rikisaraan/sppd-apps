<form class="form-horizontal" action="<?php echo  current_url()?>" method="POST">
  <div class="box box-danger">
    <div class="box-header with-border">
      <h3 class="box-title">Form User</h3>
    </div>
    <div class="box-body">
      <div class="row">      
        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">        
          <div class="box-body">            
            <div class="form-group">
              <label for="user_name" class="col-sm-4 control-label">Username</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="user_name" placeholder="" value="<?php echo $formData->user_name?>" name="user_name" required>
              </div>
            </div>
            <div class="form-group">
              <label for="password" class="col-sm-4 control-label">Passowrd</label>
              <div class="col-sm-8">                
                <input type="password" class="form-control" id="password" value="<?php echo $this->encryption->decrypt($formData->password)?>" name="password" required>
              </div>
            </div>
          </div>          
        </div>
        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">        
          <div class="box-body">            
            <div class="form-group">
              <label class="col-sm-4 control-label">Akses</label>
              <div class="col-sm-8">
                <select class="form-control select2" name="id_role" required>
                <option>Pilih Akses</option>
                <?php
                  foreach ($dataRole->result() as $role) {
                    $selected = ($role->id_role==$formData->id_role) ? "selected":"";
                    echo"<option value='".$role->id_role."' ".$selected.">".$role->nama_role."</option>";
                  }
                ?>                  
                </select>
              </div>
            </div>
          </div>                  
        </div>            
      </div>
      <div class="box-footer">      
        <button type="submit" class="btn btn-info pull-right btn-flat bg-navy">Simpan</button>
      </div>
    </div>
    <!-- /.box-body -->
  </div>
</form> 