<div class="box">
  <div class="box-header">
    <h3 class="box-title">Daftar Memo Dinas</h3>
    <a href="<?php echo site_url('admin/MemoDinas/add')?>" class="btn btn-flat bg-navy pull-right">Tambah &nbsp;<span class="fa fa-plus"></span></a>
  </div>
<!-- /.box-header -->
  <div class="box-body">
    <table id="datatable" class="table table-bordered table-striped">
      <thead>
        <tr>
          <th>Nomor Memo</th>
          <th>Pengirim</th>
          <th>Tanggal</th>
          <th>#</th>
        </tr>
      </thead>
      <tbody>
        <?php   
			//looping data untuk nampilkan data by db
          foreach($data->result() as $k) {
            echo"<tr>
                    <td>".$k->nomor_memo."</td>
                    <td>".$k->pengirim."</td>
                    <td>".$k->tanggal."</td>
                    <td align='center'>
                      <a data-toggle='tooltip' data-placement='top' title='Perbarui' href='".site_url('admin/MemoDinas/add/'.$k->id_memo)."' class='btn btn-flat bg-maroon'><span class='fa fa-edit'></span></a>                      
                      <a onclick=\"return confirm('Yakin ingin menghapus data ini?')\" data-toggle='tooltip' data-placement='top' title='Hapus' href='".site_url('admin/MemoDinas/delete/'.$k->id_memo)."' class='btn btn-flat bg-maroon'><span class='fa fa-trash'></span></a>
                      <a title='Lihat File' href='".site_url('admin/MemoDinas/showFile/'.$k->id_memo)."' class='btn btn-flat bg-maroon' data-toggle='modal' data-target='#myModal'><span class='fa fa-eye'></span></a>                      
                    </td>
                </tr>";          
          }

        ?>
      </tbody>      
    </table>
  </div>
<!-- /.box-body -->
</div>
<!-- /.box -->