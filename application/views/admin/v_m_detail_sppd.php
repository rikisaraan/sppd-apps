<div class="box box-danger">
    <div class="box-header with-border">
      <h3 class="box-title">Data Profile Staf</h3>
    </div>    
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div class="box-body">
                <table class="table table-bordered">    
                    <tbody>
                        <tr>
                            <td width="15%">Nik </td>
                            <td width="35%"><?php echo $data->nik?></td>
                            <td width="15%">Nama Karyawan</td>
                            <td width="35%"><?php echo $data->nama_karyawan?></td>                            
                        </tr>
                        <tr>
                            <td width="15%">Jabatan </td>
                            <td width="35%"><?php echo $data->nama_jabatan?></td>
                            <td width="15%">Nama Divisi</td>
                            <td width="35%"><?php echo $data->nama_divisi?></td>                            
                        </tr>                        
                    </tbody>
                </table>    
            </div>            
        </div>
    </div>
</div>

<div class="box box-danger">
    <div class="box-header with-border">
      <h3 class="box-title">Data Anggaran</h3>
    </div>    
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div class="box-body">
                <table class="table table-bordered">    
                    <tbody>
                        <?php
                            $uang_penginapan=($data->lama_perjalanan>1) ? $data->uang_penginapan:0;
                            $uang_transport=($data->uang_transport*$data->sppd_jarak);

                            $totalAnggaran=($data->uang_makan+$data->uang_saku+$uang_penginapan+$uang_transport);
                        ?>
                        <tr>
                            <td width="15%">Uang Makan </td>
                            <td width="35%"><?php echo rupiah($data->uang_makan)?></td>
                            <td width="15%">Uang Saku</td>
                            <td width="35%"><?php echo rupiah($data->uang_saku)?></td>                            
                        </tr>
                        <tr>
                            <td width="15%">Uang Penginapan </td>
                            <td width="35%"><?php echo rupiah($uang_penginapan)?></td>
                            <td width="15%">Uang Transport </td>
                            <td width="35%"><?php echo rupiah($uang_transport)?></td>
                        </tr>
                        <tr>
                            <td colspan="3"><b>Total Anggaran yang dikeluarkan</b></td>
                            <td><b><?php echo rupiah($totalAnggaran)?></b></td>
                        </tr>
                    </tbody>
                </table>    
            </div>            
        </div>
    </div>
</div>

<div class="box box-danger">
    <div class="box-header with-border">
      <h3 class="box-title">Detail Surat Perintah Perjalanan Dinas</h3>
    </div>    
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div class="box-body">
                <table class="table table-bordered">    
                    <tbody>
                        <tr>
                            <td width="15%">Tanggal Berangkat </td>
                            <td width="35%"><?php echo $data->tanggal_berangkat?></td>
                            <td width="15%">Tanggal Pulang</td>
                            <td width="35%"><?php echo $data->tanggal_pulang?></td>                            
                        </tr>
                        <tr>
                            <td width="15%">Asal </td>
                            <td width="35%"><?php echo $data->asal?></td>
                            <td width="15%">Tujuan </td>
                            <td width="35%"><?php echo $data->tujuan?></td>
                        </tr>
                        <tr>
                            <td width="15%">Lama Perjalanan </td>
                            <td width="35%"><?php echo $data->lama_perjalanan?> Hari</td>
                            <td width="15%">Transportasi </td>
                            <td width="35%"><?php echo $data->transportasi?></td>                            
                        </tr>
                        <tr>   
                            <td width="15%">No Surat </td>
                            <td width="35%"><?php echo $data->no_surat?></td>                            
                            <td width="15%">Status</td>
                            <td width="35%"><?php echo $data->sppd_status_name?></td>
                        </tr>
                    </tbody>
                </table>    
            </div>            
        </div>
    </div>
</div>
