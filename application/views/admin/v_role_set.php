<form action="<?php echo  current_url()?>" method="POST">
  <div class="box box-danger">
    <div class="box-header with-border">
      <h3 class="box-title">Form Set Role</h3>
    </div>
    <div class="box-body">
      <div class="row">      
        <?php
          foreach ($category_page->result() as $c ) {
                      $this->db->order_by("page_sort","asc");
            $qPage =  $this->db->get_where("page",array("category_id"=>$c->category_id));
            $page="";
            foreach ($qPage->result() as $p ) {
              $qUserRole = $this->db->get_where("user_role",array("page_id"=>$p->page_id,"id_role"=>$id_role));
              $check = ($qUserRole->num_rows()<1) ? "":"checked" ;

              $page.='
                  <div class="form-group">
                    <label>
                      <input value="'.$p->page_id.'" name="page_id[]" type="checkbox" class="minimal-red" '.$check.'> &nbsp;'.$p->page_label.'
                    </label>
                  </div>
              ';
            }

            echo'
              <div class="col-md-6">
                <div class="box box-warning">
                  <div class="box-header">
                    <h3 class="box-title">'.$c->category_title.'</h3>
                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                  </div>
                  <div class="box-body">'.$page.'</div>
                </div>                
              </div>        
            ';
          }
        ?>        
      </div>
    </div>    
    <div class="box-footer">
      <button type="submit" class="btn btn-info pull-right btn-flat bg-navy">Simpan</button>
    </div>
  </div>
</form> 