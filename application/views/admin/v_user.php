<div class="box">
  <div class="box-header">
    <h3 class="box-title">Daftar User</h3>
    <!-- <a href="<?php echo site_url('admin/c_user/add')?>" class="btn btn-flat bg-navy pull-right">Tambah &nbsp;<span class="fa fa-plus"></span></a> -->
  </div>
<!-- /.box-header -->
  <div class="box-body">
    <table id="datatable" class="table table-bordered table-striped">
      <thead>
        <tr>
          <th>Id user</th>
          <th>Username</th>
          <th>Role</th>
          <th>Nik</th>
          <th>#</th>
        </tr>
      </thead>
      <tbody>
        <?php          
          foreach($data->result() as $k) {
            echo"<tr>
                    <td>".$k->user_id."</td>
                    <td>".$k->user_name."</td>
                    <td>".$k->nama_role."</td>
                    <td>".$k->nik."</td>
                    <td align='center'>
                      <a data-toggle='tooltip' data-placement='top' title='Perbarui' href='".site_url('admin/c_user/add/'.$k->nik.'/'.$k->user_id)."' class='btn btn-flat bg-maroon'><span class='fa fa-edit'></span></a>
                      <a onclick=\"return confirm('Yakin ingin menghapus data ini?')\" data-toggle='tooltip' data-placement='top' title='Hapus' href='".site_url('admin/c_user/delete/'.$k->user_id)."' class='btn btn-flat bg-maroon'><span class='fa fa-trash'></span>
                    </td>
                </tr>";
          }

        ?>
      </tbody>      
    </table>
  </div>
<!-- /.box-body -->
</div>
<!-- /.box -->