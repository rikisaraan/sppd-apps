<?php                  
    $rowKaryawan="";
    foreach ($dataKaryawan->result() as $karyawan) {                    
      $rowKaryawan .="<option value='".$karyawan->nik."' >".$karyawan->nik." - ".$karyawan->nama_karyawan."</option>";
    }
?>

<form class="form-horizontal" action="<?php echo  current_url()?>" method="POST" enctype="multipart/form-data">
  <div class="box box-danger">
    <div class="box-header with-border">
      <h3 class="box-title">Form Surat Perintah Tugas</h3>
    </div>
    <div class="box-body">
      <div class="row">      
        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">        
          <div class="box-body">    
            <div class="form-group">
                <label class="col-sm-4 control-label">Memo Dinas</label>
                <div class="col-sm-8">
                    <select class="form-control select2" name="id_memo">                  
                        <option  value="">Pilih Memo Dinas</option>
                        <?php                  
                            foreach ($dataMemoDinas->result() as $memoDinas) {                    
                                echo"<option value='".$memoDinas->id_memo."' >".$memoDinas->nomor_memo." - ".$memoDinas->pengirim."</option>";
                            }
                        ?>                  
                    </select>
                </div>
            </div>                    
            <div class="form-group">
              <label for="tanggal" class="col-sm-4 control-label">Tanggal</label>
              <div class="col-sm-8">
                <input type="text" readonly class="datepicker form-control" id="tanggal" placeholder="YYYY-MM-DD" value="<?php echo $formData->tanggal?>" name="tanggal" required >
              </div>
            </div>
            <div class="form-group">
              <label for="lama" class="col-sm-4 control-label">Lama Tugas (Hari)</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="lama" placeholder="Contoh : 4" value="<?php echo $formData->lama?>" name="lama" required>
              </div>
            </div>
            
          </div>          
        </div>
        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">        
          <div class="box-body">            
            <div class="form-group">
              <label for="tempat" class="col-sm-4 control-label">Tempat Tugas</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="tempat" placeholder="Contoh : Kota Binjai" value="<?php echo $formData->tempat?>" name="tempat" required>
              </div>
            </div>
            <div class="form-group">
              <label for="kegiatan" class="col-sm-4 control-label">Kegiatas Tugas</label>
              <div class="col-sm-8">
                <textarea name="kegiatan" class="form-control" placeholder="Contoh : Memperbaiki sistem POS" rows="5" required><?php echo $formData->kegiatan?></textarea>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
          <div class="col-sm-12 col-md-12 col-lg-8 col-xl-8 col-md-offset-2">
              <div class="box-body">
                <table class="table table-bordered">
                  <thead>                                            
                      <tr>
                        <th colspan="2"><strong>Pilih Staf yang ditugaskan :</strong></th>
                      </tr>
                      <tr>
                          <th>Nama</th>
                          <th style="text-align:center" width="10%">
                              <a href="#" id="add-new" class="btn btn-success"><span class="fa fa-plus"></span></a>
                          </th>
                      </tr>
                  </thead>
                  <tbody class="pos-table" id="trans_det_list"></tbody>
              </table>
              </div>
          </div>
      </div>
      <div class="box-footer">      
        <button type="submit" class="btn btn-info pull-right btn-flat bg-navy">Simpan</button>
      </div>
    </div>
    <!-- /.box-body -->
  </div>
</form> 

<script>

var transDetList = <?php echo json_encode(' <tr id="{id}" class="pos-row">                                                    
                                                    <td>
                                                        <select class="form-control select2" name="nik[]" id="nik{id}" width="100%"> 
                                                            <option  value="">Pilih Karyawan</option>
                                                            '.$rowKaryawan.'                    
                                                        </select>
                                                    </td>
                                                    <td style="text-align:center">
                                                        <a href="#" class="remove btn btn-danger" id=""><span class="fa fa-times"></span></a>
                                                    </td>
                                                </tr>')?>;
    
    var id = 0;
        
    $(document).ready(function() {
        if(id < 1)$("#add-new").trigger("click");
        $("#show").trigger("click");        
    });
        
    
    var tr = $('.pos-table').html();    
    var tamBah = function()
    {        
        $('.pos-table').append(tr);            
        var item=1;
        $.each($('#pos-table').children('tr'),function(index,obj){
            var staf=$('input',$(obj).children('td').eq(0));            
            if(staf.val() !=''){
                item++;               
            }

        });
        
    }
    
    //event ketika enter dan menambah row
    $(document).on('click','#add-new',function(e){    
        e.preventDefault();
        
        id++;
        var newID =id;
        
        $("#trans_det_list").append(transDetList.replace(/{id}/gi,newID));   
        //$("#nik"+id).attr("class","select2");
        $('.select2').select2();
        tamBah();         
    });
    
    $(document).on("click",".remove",function(e){
        e.preventDefault();
        if(window.confirm("anda yakin akan Peneriman surat ini ?"))
        $(this).parent().parent().remove();
     
    });
   
</script>