<form action="<?php echo current_url()?>" method="post">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title"><?php echo $title?></h4>
	</div>
	<div class="modal-body">
		<?php
			if(isset($body)):
			
				$this->load->view($body,$data);
			
			elseif(isset($content)):
				echo $content;
			
			endif;
		?>
		
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
		<?php if(isset($footer) && $footer):?>
		<button type="submit" class="btn btn-primary" data-submit="#myModal">OK</button>
		<?php endif ?>
	</div>
</form>
